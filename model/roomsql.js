const sqlCon = require('../sqlconfig');
var Player_points = require('../Classes/PlayerPoints');
var Player_papludp = require('../Classes/PlayerPapluDp');

var Player_deal = require('../Classes/PlayerDeal');
var Player_pool = require('../Classes/PlayerPool');
var Player_onecard= require('../Classes/PlayerOneCard')

const Enum = require('enum');

var dateFormat = require('dateformat');

//var Maxtype = new Enum(['2player', '6player']);
const Maxtype = new Enum({'twoplayer':2, 'sixplayer':6, 'tenplayer':10});
const Gametype = new Enum({'Points':1, 'Pool':2, 'Deal':3, 'Raise':4 , 'PapluDp':5, 'OneCard':6});
const Point_table_type = new Enum({'0.5':0.5,'1':1.0, '5':5.0, '10':10.0,'100':100.0});
const Moneytype = new Enum({'free':0, 'paid':1});
//const OneCardMaxtype = new Enum({'tenplayer':10});
//const Amounttype = new Enum({'':1, 'Pool':2, 'Deal':3, 'Raise':4 , 'PapluDp':5, 'OneCard':6});
function gettime1() {
 
  var there = new Date();
  // suppose the date is 12:00 UTC
  var invdate = new Date(there.toLocaleString('en-US', {
    timeZone: "Asia/Calcutta"
  }));
	console.log(dateFormat(invdate, "yyyy-mm-dd HH:MM:ss"))
  return dateFormat(invdate, "yyyy-mm-dd HH:MM:ss")

  // then invdate will be 07:00 in Toronto
  // and the diff is 5 hours
  var diff = there.getTime() - invdate.getTime();

  // so 12:00 in Toronto is 17:00 UTC
  return (new Date(there.getTime() + diff));

}

//var thismaxtype = Moneytype.get('2player')  //get index
//var thimaxtypevalu = Maxtype2.get(2).key  //search value
module.exports.hasEnoughBalance = function hasEnoughBalance(uid,expectedAmt,socketid,moneytype,callback){
  if(moneytype == "free"){
    var stm = 'SELECT coins FROM `register`  WHERE u_id = '+uid+''
    sqlCon.query(stm, function (err, result)
     {
      if (err) {return console.log(err)}//throw err;
      if(result.length == 0) 
      {
        callback(false,socketid)
        return
      }
      else if(result[0].coins   >= expectedAmt){
        callback(true,socketid)
      }
      else{
        callback(false,socketid)
      }
    });
  }
  else{
    var stm = 'SELECT winning_amount,amount FROM `register`  WHERE u_id = '+uid+''
    sqlCon.query(stm, function (err, result)
     {
      if (err) {return console.log(err)}//throw err;
      if(result.length == 0) 
      {
        callback(false,socketid)
        return
      }
      else if(result[0].winning_amount + result[0].amount  >= expectedAmt){
        callback(true,socketid)
      }
      else{
        callback(false,socketid)
      }
    });
  }
 
}
module.exports.getuserbalance = function getvalues(uid,callback){
  var stm = 'SELECT winning_amount,amount FROM `register`  WHERE u_id = '+uid+''
  sqlCon.query(stm, function (err, result)
   {
    if (err) {return console.log(err)}//throw err;
    if(result.length == 0) 
    {
      return
    }
    
    else{
      callback(result[0].winning_amount + result[0].amount)
    }
  });
}

module.exports.getvalues = function getvalues(callback){
  var point =null
  var deal = null 
  var raise =null
  var pool = null
  var papludp = null
  var onecard = null
console.log('i am in query');
  var stm = 'SELECT id FROM `room_points`  WHERE id = (SELECT MAX(id) FROM room_points)'
    sqlCon.query(stm, function (err, result) {
      if (err) {return console.log(err)}//throw err;
      result.length > 0 ? point = result[result.length-1].id : point = 0

      if(point!= null && deal!= null && pool!= null && raise!= null && papludp != null && onecard!=null)
      { 
        callback(point,deal,pool,raise,papludp,onecard)
     }
    });
    var stm2 = 'SELECT id FROM `room_deal`  WHERE id = (SELECT MAX(id) FROM room_deal)'
    sqlCon.query(stm2, function (err, result) {
      if (err) {return console.log(err)}//throw err;
      result.length > 0 ? deal = result[result.length-1].id : deal = 0
      
      if(point!= null && deal!= null && pool!= null && raise!= null && papludp != null && onecard!=null)
      { 
        callback(point,deal,pool,raise,papludp,onecard)
     }
    });
    var stm3 = 'SELECT id FROM `room_pool`  WHERE id = (SELECT MAX(id) FROM room_pool)'
    sqlCon.query(stm3, function (err, result) {
      if (err) {return console.log(err)}//throw err;
      result.length > 0 ? pool = result[result.length-1].id : pool =0
      
      if(point!= null && deal!= null && pool!= null && raise!= null && papludp != null && onecard!=null)
      { 
        callback(point,deal,pool,raise,papludp,onecard)
     }
    });
    var stm4 = 'SELECT id FROM `room_raise`  WHERE id = (SELECT MAX(id) FROM room_raise)'
    sqlCon.query(stm4, function (err, result) {
      if (err) {return console.log(err)}//throw err;
      result.length > 0 ? raise = result[result.length-1].id : raise = 0
      
      if(point!= null && deal!= null && pool!= null && raise!= null && papludp != null && onecard!=null)
      { 
        callback(point,deal,pool,raise,papludp, onecard)
     }
    });

    var stm5 = 'SELECT id FROM `room_papludp`  WHERE id = (SELECT MAX(id) FROM room_papludp)'
    sqlCon.query(stm5, function (err, result) {
      if (err) {return console.log(err)}//throw err;
      result.length > 0 ? papludp = result[result.length-1].id : papludp = 0
      
      if(point!= null && deal!= null && pool!= null && raise!= null && papludp != null && onecard!=null)
      { 
        callback(point,deal,pool,raise,papludp,onecard)
     }
    });

  var stm6 = 'SELECT id FROM `room_onecard`  WHERE id = (SELECT MAX(id) FROM room_onecard)'
  sqlCon.query(stm6, function (err, result) {
    if (err) {return console.log(err)}//throw err;
    result.length > 0 ? onecard = result[result.length-1].id : onecard = 0
     console.log('Onecard from stm6',stm6);
    if(point!= null && deal!= null && pool!= null && raise!= null && papludp != null && onecard != null)
    {
      callback(point,deal,pool,raise,papludp,onecard)
    }
  });

    var stmdeleteoldlive = 'DELETE FROM `live`'
    sqlCon.query(stmdeleteoldlive, function (err, result) {
      console.log("old live data deleted")
    });
    var stmcloseroom1 = 'UPDATE `room_points` SET `closed` = 1'
    sqlCon.query(stmcloseroom1, function (err, result) {
      console.log("closing room_points rooms")
    });
    var stmcloseroom2 = 'UPDATE `room_pool` SET `closed` = 1'
    sqlCon.query(stmcloseroom2, function (err, result) {
      console.log("closing room_pool rooms")
    });
    var stmcloseroom3 = 'UPDATE `room_deal` SET `closed` = 1'
    sqlCon.query(stmcloseroom3, function (err, result) {
      console.log("closing room_deal rooms")
    });
    var stmcloseroom4 = 'UPDATE `room_raise` SET `closed` = 1'
    sqlCon.query(stmcloseroom4, function (err, result) {
      console.log("closing room_raise rooms")
    });
    var stmcloseroom5 = 'UPDATE `room_papludp` SET `closed` = 1'
    sqlCon.query(stmcloseroom5, function (err, result) {
      console.log("closing room_papludp rooms")
    });
  var stmcloseroom6 = 'UPDATE `room_onecard` SET `closed` = 1'
  sqlCon.query(stmcloseroom6, function (err, result) {
    console.log("closing room_onecard rooms")
  });

    
    


   // deleteolddata()  
}
function deleteolddata(){
    console.log("deleteling old data")
    sqlCon.query("delete from room_papludp", function (err, result) {});
    sqlCon.query("delete from room_raise", function (err, result) {});
    sqlCon.query("delete from room_deal", function (err, result) {});
    sqlCon.query("delete from room_pool", function (err, result) {});
    sqlCon.query("delete from room_points", function (err, result) {});
    sqlCon.query("delete from matchlog_deal", function (err, result) {});
    sqlCon.query("delete from matchlog_raise", function (err, result) {});
    sqlCon.query("delete from matchlog_papludp", function (err, result) {});
    sqlCon.query("delete from matchlog_pool", function (err, result) {});
    sqlCon.query("delete from matchlog_points", function (err, result) {});
    sqlCon.query("delete from player_results", function (err, result) {});
}
function insertPointRoom(id,maxtype ,moneytype,tabletype,  callback)  {
  console.log('jbhbhj',id);
    const max = Maxtype.get(maxtype).value
    const money = Moneytype.get(moneytype).value
    const table =  parseFloat(tabletype)//Point_table_type.get(tabletype).value

  
    var stm = 'INSERT INTO `room_points` (`id`, `max_player`, `table_type`, `money_type`, `closed`, `created_at`) VALUES ('+id+', '+max+', '+table+', '+money+', 0, \''+gettime1()+'\')'

    sqlCon.query(stm, function (err, result) {
        if (err) {return console.log(err)}//throw err; 

        return callback(null,result)
       

      });
} 
module.exports.insertPointRoom = insertPointRoom
function insertPapluDpRoom(id,maxtype ,moneytype,tabletype,  callback)  {
  const max = Maxtype.get(maxtype).value
  const money = Moneytype.get(moneytype).value
  const table =  parseFloat(tabletype)//Point_table_type.get(tabletype).value


  var stm = 'INSERT INTO `room_papludp` (`id`, `max_player`, `table_type`, `money_type`, `closed`, `created_at`) VALUES ('+id+', '+max+', '+table+', '+money+', 0, \''+gettime1()+'\')'

  sqlCon.query(stm, function (err, result) {
      if (err) {return console.log(err)}//throw err; 

      return callback(null,result)
     

    });
} 
module.exports.insertPapluDpRoom = insertPapluDpRoom
function insertDealRoom(id,maxtype ,moneytype,tabletype,entry_fee,  callback)  {
  const max = Maxtype.get(maxtype).value
  const money = Moneytype.get(moneytype).value
  const table =  parseFloat(tabletype)//Point_table_type.get(tabletype).value


  var stm = 'INSERT INTO `room_deal` (`id`, `max_player`, `deal_count`, `entry_fee`, `money_type`, `closed`, `created_at`) VALUES ('+id+', '+max+', '+table+', '+entry_fee+', '+money+', 0, \''+gettime1()+'\')'

  sqlCon.query(stm, function (err, result) {
      if (err) {return console.log(err)}//throw err;

      return callback(null,result)
     

    });
} 
module.exports.insertDealRoom = insertDealRoom
function insertPoolRoom(id,maxtype ,moneytype,tabletype,entry_fee,  callback)  {

  const max = Maxtype.get(maxtype).value
  const money = Moneytype.get(moneytype).value
  const table =  parseFloat(tabletype)//Point_table_type.get(tabletype).value


  var stm = 'INSERT INTO `room_pool` (`id`, `max_player`, `max_points`, `entry_fee`, `money_type`, `closed`, `created_at`) VALUES ('+id+', '+max+', '+table+', '+entry_fee+', '+money+', 0, \''+gettime1()+'\')'

  sqlCon.query(stm, function (err, result) {
      if (err) {return console.log(err)}//throw err; 

      return callback(null,result)
     

    });
} 
module.exports.insertPoolRoom = insertPoolRoom
function insertOneCardRoom(id, maxtype, moneytype, tabletype, callback)  {
  console.log("in roomsql--------------------------------------------------",id);
  const max = Maxtype.get(maxtype).value
  const money = Moneytype.get(moneytype).value
  const table =  parseFloat(tabletype)//Point_table_type.get(tabletype).value
  var stm = 'INSERT INTO `room_onecard` (`id`, `max_player`, `table_type`, `money_type`, `closed`, `created_at`) VALUES ('+id+', '+max+', '+table+', '+money+', 0, \''+gettime1()+'\')'
  sqlCon.query(stm, function (err, result) {
    if (err) {return console.log(err)}//throw err;

    return callback(null,result)


  });
}
module.exports.insertOneCardRoom = insertOneCardRoom
module.exports.updateRoomClosed = function updateRoomClosed(id,gametype)
{
     var stm
    switch (gametype)
    {
      case Gametype.get(1).key:
        stm = 'UPDATE `room_points` SET `closed` = 1 WHERE `room_points`.`id` = '+id+''
        break;
      case Gametype.get(2).key:
        stm = 'UPDATE `room_pool` SET `closed` = 1 WHERE `room_pool`.`id` = '+id+''
        break;
      case Gametype.get(3).key:
        stm = 'UPDATE `room_deal` SET `closed` = 1 WHERE `room_deal`.`id` = '+id+''
        break;

      case Gametype.get(4).key:
        stm = 'UPDATE `room_raise` SET `closed` = 1 WHERE `room_raise`.`id` = '+id+''
        break;
      
      case Gametype.get(5).key:
        stm = 'UPDATE `room_papludp` SET `closed` = 1 WHERE `room_papludp`.`id` = '+id+''
        break;
      case Gametype.get(6).key:
        stm = 'UPDATE `room_onecard` SET `closed` = 1 WHERE `room_onecard`.`id` = '+id+''
        break;
      }
      sqlCon.query(stm, function (err, result) {
        if (err) {return console.log(err)}//throw err;

        console.log("room closed updated in database")
        


      });

   
}

module.exports.inserplayerinlive = function inserplayerinlive(username,gametype,maxtype,moneytype,tabletype,entryfee) //
{
 var  points,deal,pool
  switch (gametype)
    {
      case Gametype.get(1).key:
        points = tabletype
        deal = 0
        pool = 0
        entryfee = 0
        break;
      case Gametype.get(2).key:
        points = 0
        deal = 0
        pool = tabletype
        break;
      case Gametype.get(3).key:
        points = 0
        deal = tabletype
        pool = 0
        break;

      case Gametype.get(4).key:
        points = tabletype
        deal = 0
        pool = 0
        entryfee = 0
        break;
      
      case Gametype.get(5).key:
        points = tabletype
        deal = 0
        pool = 0
        entryfee = 0
        break;

      case Gametype.get(6).key:
        points = tabletype
        deal = 0
        pool = 0
        entryfee = 0
        break;
      }
      //  //INSERT INTO `live` (`username`, `game_type`, `max_player`, `money_type`, `points`, `pool`, `deal`, `entryfee`) VALUES ('user12', 'Points', '2', 'Paid', '10', '0', '0', '0');

  var stm = 'INSERT INTO `live` (`username`, `game_type`, `max_player`, `money_type`, `points`, `pool`, `deal`, `entryfee`) VALUES (\''+username+'\',\''+gametype+'\',\''+maxtype+'\',\''+moneytype+'\',\''+points+'\',\''+pool+'\',\''+deal+'\',\''+entryfee+'\')'
  sqlCon.query(stm, function (err, result) {
    if (err) {return console.log(err)}//throw err;
    console.log('insert player database: ')
  });
   
}
module.exports.deleteplayerfromlive = function deleteplayerfromlive(username)
{
  var stm =  'DELETE FROM `live` WHERE `live`.`username` = \''+username+'\''

  sqlCon.query(stm, function (err, result) {
    if (err) {return console.log(err)}//throw err;
    console.log(' player deleted in database: ')
  });
   
}


module.exports.insertgameresult_points = function insertgameresult(maxtype,tabletype,money_type,roomnumber,players,playersleft,winner_u_id,totalwinningpoints)
{//6 max  'paid'moneytype  0.1 tabletype
  var playerescoppy = []
  for(var i= 0 ; i<players.length;i++){
    var temp =  new Player_points(null,players[i].uid,players[i].username,null,maxtype,money_type,tabletype,players[i].dname,players[i].autodrop)
    temp.mypoints = players[i].mypoints
    playerescoppy.push(temp)
      
    }
  var playersleftcopy = [] 
  for(var i= 0 ; i<playersleft.length;i++){
    var temp =  new Player_points(null,playersleft[i].uid,playersleft[i].username,null,maxtype,money_type,tabletype,playersleft[i].dname,playersleft[i].autodrop)
    temp.mypoints = playersleft[i].mypoints
    playersleftcopy.push(temp)
  } 
  var total_winning_amt = tabletype *  totalwinningpoints
  var deduction = 0

  var bonus = 0

  var monetypeint = 0
  if (money_type == 'paid'){
    monetypeint = 1
    if(maxtype == 6){
      deduction = Math.round((10/100) * total_winning_amt * 100)/100  //6 player 10 %
      bonus = Math.round((10/100) * deduction)
    }
    else{
      deduction = Math.round((10/100) * total_winning_amt * 100)/100  //2 player 10 %
      bonus = Math.round((10/100) * deduction)

    }
  }
  var winning_amt = total_winning_amt - deduction

  
//INSERT INTO `matchlog_points` (`match_id`, `room_id`, `winner_u_id`, `win_points`, `table_type`, `total_winning_amt`, `winning_amt`, `deduction`, `money_type`, `created_at`) VALUES (NULL, '1', '37', '60', '10', '600', '510', '90', '1', \''+gettime1()+'\');
   stm = 'INSERT INTO `matchlog_points` (`match_id`, `room_id`, `winner_u_id`, `win_points`, `table_type`, `total_winning_amt`, `winning_amt`, `deduction`, `money_type`, `created_at`,`max_player`) VALUES (NULL, '+roomnumber+', '+winner_u_id+', '+totalwinningpoints+', '+tabletype+', '+total_winning_amt+', '+winning_amt+', '+deduction+', '+monetypeint+', \''+gettime1()+'\','+maxtype+');'
  //todo
      sqlCon.query(stm, function (err, result) {
        if (err) {return console.log(err)}//throw err;

        console.log("machlog entry added",result.insertId)
        // for(z=0 ; z<playerescoppy.length ; z++)
        // {
        //   //var amt = playerescoppy[i].mypoints * tabletype
        //   // var minus = 100
        //   // stm2 = 'INSERT INTO `player_results` (`id`, `u_id`, `match_id`, `money_type`, `amount`, `created_at`, `remain_deposit`, `Remain_winning`) VALUES (NULL, '+playerescoppy[z].uid+', '+result.insertId+', '+monetypeint+', '+minus+', \''+gettime1()+'\', 0, 0)'
        //   // sqlCon.query(stm2, function (err, result){} )
        //   //deduct_money_from_player(playerescoppy[z].uid,amt,money_type)
        //   add_money_to_player(playerescoppy[z].uid,(winning_amt + amttoaddsubtract +100 ),money_type,bonus,0)
        // }
        // console.log("lenght" +players.length)
        // for(var x =0 ; x<players.length ; x++)
        // {
        //    console.log("for staRT")
        //  stm2 = 'INSERT INTO `player_results` (`id`, `u_id`, `match_id`, `money_type`, `amount`, `created_at`, `remain_deposit`, `Remain_winning`) VALUES (NULL, '+playerescoppy[x].uid+', '+result.insertId+', '+monetypeint+', '+winning_amt+', \''+gettime1()+'\', 0, 0)'
        //  console.log("mid2")
        //  sqlCon.query(stm2, function (err, result){} )
        //  var amttoaddsubtract = (80 * tabletype) 
        //  console.log("midfor")
        //  add_money_to_player(players[x].uid,100 ,money_type,bonus,amttoaddsubtract)
        //  console.log("for end")
        // }

        for(var i= 0 ; i<playerescoppy.length;i++)
        {
          console.log("for end")
          if(playerescoppy[i].uid == winner_u_id)
          {
            console.log("winner")
            stm2 = 'INSERT INTO `player_results` (`id`, `u_id`, `match_id`, `money_type`, `amount`, `created_at`, `remain_deposit`, `Remain_winning`) VALUES (NULL, '+playerescoppy[i].uid+', '+result.insertId+', '+monetypeint+', '+winning_amt+', \''+gettime1()+'\', 0, 0)'
            sqlCon.query(stm2, function (err, result){} )
            var amttoaddsubtract = (80 * tabletype) 
            add_money_to_player(playerescoppy[i].uid,(winning_amt + amttoaddsubtract /*+100*/ ),money_type,bonus,amttoaddsubtract)   
            //deduct_money_from_player(playerescoppy[i].uid,amttoaddsubtract,money_type)
          }
          else{
            var amt = playerescoppy[i].mypoints * tabletype
            var minus = 0 - amt
            stm2 = 'INSERT INTO `player_results` (`id`, `u_id`, `match_id`, `money_type`, `amount`, `created_at`, `remain_deposit`, `Remain_winning`) VALUES (NULL, '+playerescoppy[i].uid+', '+result.insertId+', '+monetypeint+', '+minus+', \''+gettime1()+'\', 0, 0)'
            sqlCon.query(stm2, function (err, result){} )
            deduct_money_from_player(playerescoppy[i].uid,amt,money_type)

          }
        }
        for(var i= 0 ; i<playersleftcopy.length;i++){
          var amt = (playersleftcopy[i].mypoints * tabletype )           //-100
          var minus = 0 - amt 
          stm2 = 'INSERT INTO `player_results` (`id`, `u_id`, `match_id`, `money_type`, `amount`, `created_at`, `remain_deposit`, `Remain_winning`) VALUES (NULL, '+playersleftcopy[i].uid+', '+result.insertId+', '+monetypeint+', '+minus+', \''+gettime1()+'\', 0, 0)'
          sqlCon.query(stm2, function (err, result){} )
          deduct_money_from_player(playersleftcopy[i].uid,amt,money_type)
        }

        
      });
     
   
}
module.exports.insertgameresult_Papludp = function insertgameresult_Papludp(maxtype,tabletype,money_type,roomnumber,players,playersleft,winner_u_id,totalwinningpoints,wintype)
{//6 max  'paid'moneytype  0.1 tabletype
  var playerescoppy = []
  for(var i= 0 ; i<players.length;i++){
    var temp =  new Player_papludp(null,players[i].uid,players[i].username,null,maxtype,money_type,tabletype,players[i].dname,players[i].autodrop)
    temp.mypoints = players[i].mypoints
    playerescoppy.push(temp)
    }
  var playersleftcopy = [] 
  for(var i= 0 ; i<playersleft.length;i++){
    var temp =  new Player_papludp(null,playersleft[i].uid,playersleft[i].username,null,maxtype,money_type,tabletype,playersleft[i].dname,playersleft[i].autodrop)
    temp.mypoints = playersleft[i].mypoints
    playersleftcopy.push(temp)
  } 
  var total_winning_amt = tabletype *  totalwinningpoints
  var deduction = 0
  var bonus = 0
  var monetypeint = 0
  if (money_type == 'paid'){
    monetypeint = 1
    if(maxtype == 6){
      deduction = Math.round((10/100) * total_winning_amt * 100)/100  //6 player 12 %
      bonus = Math.round((10/100) * deduction)
    }
    else{
      deduction = Math.round((10/100) * total_winning_amt * 100)/100  //2 player 12 %
      bonus = Math.round((10/100) * deduction)

    }
  }
  var winning_amt = total_winning_amt - deduction

  
//INSERT INTO `matchlog_points` (`match_id`, `room_id`, `winner_u_id`, `win_points`, `table_type`, `total_winning_amt`, `winning_amt`, `deduction`, `money_type`, `created_at`) VALUES (NULL, '1', '37', '60', '10', '600', '510', '90', '1', \''+gettime1()+'\');
   stm = 'INSERT INTO `matchlog_papludp` (`match_id`, `room_id`, `winner_u_id`, `win_points`, `table_type`, `total_winning_amt`, `winning_amt`, `deduction`, `money_type`, `created_at`,`win_type`,`max_player`) VALUES (NULL, '+roomnumber+', '+winner_u_id+', '+totalwinningpoints+', '+tabletype+', '+total_winning_amt+', '+winning_amt+', '+deduction+', '+monetypeint+', \''+gettime1()+'\','+wintype+','+maxtype+');'
  //todo
      sqlCon.query(stm, function (err, result) {
        if (err) {return console.log(err)}//throw err;

        console.log("machlog entry added",result.insertId)
        

        for(var i= 0 ; i<playerescoppy.length;i++){
          if(playerescoppy[i].uid == winner_u_id)
          {
            stm2 = 'INSERT INTO `player_results` (`id`, `u_id`, `match_id`, `money_type`, `amount`, `created_at`, `remain_deposit`, `Remain_winning`) VALUES (NULL, '+playerescoppy[i].uid+', '+result.insertId+', '+monetypeint+', '+winning_amt+', \''+gettime1()+'\', 0, 0)'
            sqlCon.query(stm2, function (err, result){} )
            var amttoaddsubtract = 120 * tabletype
            add_money_to_player(playerescoppy[i].uid,winning_amt + amttoaddsubtract,money_type,bonus,amttoaddsubtract)
            //deduct_money_from_player(playerescoppy[i].uid,amttoaddsubtract,money_type)
          }
          else{
            var amt = playerescoppy[i].mypoints * tabletype
            var minus = 0 - amt
            stm2 = 'INSERT INTO `player_results` (`id`, `u_id`, `match_id`, `money_type`, `amount`, `created_at`, `remain_deposit`, `Remain_winning`) VALUES (NULL, '+playerescoppy[i].uid+', '+result.insertId+', '+monetypeint+', '+minus+', \''+gettime1()+'\', 0, 0)'
            sqlCon.query(stm2, function (err, result){} )
            deduct_money_from_player(playerescoppy[i].uid,amt,money_type)
          }
        }
        for(var i= 0 ; i<playersleftcopy.length;i++){
          var amt = playersleftcopy[i].mypoints * tabletype
          var minus = 0 - amt
          stm2 = 'INSERT INTO `player_results` (`id`, `u_id`, `match_id`, `money_type`, `amount`, `created_at`, `remain_deposit`, `Remain_winning`) VALUES (NULL, '+playersleftcopy[i].uid+', '+result.insertId+', '+monetypeint+', '+minus+', \''+gettime1()+'\', 0, 0)'
          sqlCon.query(stm2, function (err, result){} )
          deduct_money_from_player(playersleftcopy[i].uid,amt,money_type)
        }

      });
     
   
}
module.exports.insertgameresult_deals = function insertgameresult_deals(maxtype,tabletype,money_type,roomnumber,players,playersleft,entry_fee)
{//6 max  'paid'moneytype  0.1 tabletype
  var playerescoppy = []
  for(var i= 0 ; i<players.length;i++){
    var temp =  new Player_deal(null,players[i].uid,players[i].username,null,maxtype,money_type,tabletype,entry_fee,players[i].dname,players[i].autodrop)
    temp.mypoints = players[i].mypoints
    playerescoppy.push(temp)
    }
  var playersleftcopy = [] 
  for(var i= 0 ; i<playersleft.length;i++){
    var temp =  new Player_deal(null,playersleft[i].uid,playersleft[i].username,null,maxtype,money_type,tabletype,entry_fee,playersleft[i].dname,playersleft[i].autodrop)
    temp.mypoints = playersleft[i].mypoints
    playersleftcopy.push(temp)
  } 
  var total_winning_amt = entry_fee *  (players.length+playersleft.length)

  //get winner{
    var winnerindex = 0
    for(var i= 1 ; i<players.length;i++){
      var firsttotal = players[winnerindex].gettotalpoints()
      var thistotal = players[i].gettotalpoints()
      if(thistotal < firsttotal){
        winnerindex = i
      }
    }
    var winner_u_id =  players[winnerindex].uid

  

  var deduction = 0
  var bonus = 0
  var monetypeint = 0
  if (money_type == 'paid'){
    monetypeint = 1
    if(maxtype == 6){
      deduction = Math.round((10/100) * (entry_fee * (players.length+playersleft.length)) * 100)/100  //6 player 12 %
      bonus = Math.round((10/100) * deduction)
    }
    else{
      deduction = Math.round((10/100) * (entry_fee * (players.length+playersleft.length)) * 100)/100  //2 player 12 %
      bonus = Math.round((10/100) * deduction)
    }
  }
  var winning_amt = (entry_fee * (players.length+playersleft.length)) - deduction 


  
//INSERT INTO `matchlog_points` (`match_id`, `room_id`, `winner_u_id`, `win_points`, `table_type`, `total_winning_amt`, `winning_amt`, `deduction`, `money_type`, `created_at`) VALUES (NULL, '1', '37', '60', '10', '600', '510', '90', '1', \''+gettime1()+'\');
   stm = 'INSERT INTO `matchlog_deal` (`match_id`, `room_id`, `winner_u_id`, `entry_fee`, `deal_count`, `total_winning_amt`, `winning_amount`, `deduction`, `money_type`, `created_at`,`max_player`) VALUES (NULL, '+roomnumber+', '+winner_u_id+', '+entry_fee+', '+tabletype+', '+total_winning_amt+', '+winning_amt+', '+deduction+', '+monetypeint+', \''+gettime1()+'\','+maxtype+');'
  //todo
      sqlCon.query(stm, function (err, result) {
        if (err) {return console.log(err)}//throw err;
        console.log("machlog entry added",result.insertId)
        for(var i= 0 ; i<playerescoppy.length;i++){
          if(playerescoppy[i].uid == winner_u_id)
          {
            stm2 = 'INSERT INTO `player_results` (`id`, `u_id`, `match_id`, `money_type`, `amount`, `created_at`, `remain_deposit`, `Remain_winning`) VALUES (NULL, '+playerescoppy[i].uid+', '+result.insertId+', '+monetypeint+', '+winning_amt+', \''+gettime1()+'\', 0, 0)'
            sqlCon.query(stm2, function (err, result){} )
            add_money_to_player(playerescoppy[i].uid,winning_amt,money_type,bonus,entry_fee)
            //deduct_money_from_player(playerescoppy[i].uid,entry_fee,money_type)
          }
          else{
            var minus = 0 - entry_fee
            stm2 = 'INSERT INTO `player_results` (`id`, `u_id`, `match_id`, `money_type`, `amount`, `created_at`, `remain_deposit`, `Remain_winning`) VALUES (NULL, '+playerescoppy[i].uid+', '+result.insertId+', '+monetypeint+', '+minus+', \''+gettime1()+'\', 0, 0)'
            sqlCon.query(stm2, function (err, result){} )
            deduct_money_from_player(playerescoppy[i].uid,entry_fee,money_type)
          }
        }
        for(var i= 0 ; i<playersleftcopy.length;i++){
          var minus = 0 - entry_fee
          stm2 = 'INSERT INTO `player_results` (`id`, `u_id`, `match_id`, `money_type`, `amount`, `created_at`, `remain_deposit`, `Remain_winning`) VALUES (NULL, '+playersleftcopy[i].uid+', '+result.insertId+', '+monetypeint+', '+minus+', \''+gettime1()+'\', 0, 0)'
          sqlCon.query(stm2, function (err, result){} )
          deduct_money_from_player(playersleftcopy[i].uid,entry_fee,money_type)
        }

      });
     
}

module.exports.insertgameresult_pool = function insertgameresult_pool(maxtype,tabletype,money_type,roomnumber,players,playersleft,entry_fee)
{//6 max  'paid'moneytype  0.1 tabletype
  var playerescoppy = []
  for(var i= 0 ; i<players.length;i++){
    var temp =  new Player_pool(null,players[i].uid,players[i].username,null,maxtype,money_type,tabletype,entry_fee,players[i].dname,players[i].autodrop)
    temp.mypoints = players[i].mypoints
    playerescoppy.push(temp)
    }
  var playersleftcopy = [] 
  for(var i= 0 ; i<playersleft.length;i++){
    var temp =  new Player_pool(null,playersleft[i].uid,playersleft[i].username,null,maxtype,money_type,tabletype,entry_fee,playersleft[i].dname,playersleft[i].autodrop)
    temp.mypoints = playersleft[i].mypoints
    playersleftcopy.push(temp)
  } 
  var total_winning_amt = entry_fee *  (players.length+playersleft.length)

  
    var winner_u_id =  players[0].uid

  

  var deduction = 0
  var bonus = 0
  var monetypeint = 0
  if (money_type == 'paid'){
    monetypeint = 1
    if(maxtype == 6){
      deduction = Math.round((10/100) * (entry_fee * (players.length+playersleft.length)) * 100)/100  //6 player 10 %
      bonus = Math.round((10/100) * deduction)
    }
    else{
      deduction = Math.round((10/100) * (entry_fee * (players.length+playersleft.length)) * 100)/100  //2 player 10 %
      bonus = Math.round((10/100) * deduction)
    }
  }
  var winning_amt = (entry_fee * (players.length+playersleft.length)) - deduction 


  
//INSERT INTO `matchlog_points` (`match_id`, `room_id`, `winner_u_id`, `win_points`, `table_type`, `total_winning_amt`, `winning_amt`, `deduction`, `money_type`, `created_at`) VALUES (NULL, '1', '37', '60', '10', '600', '510', '90', '1', \''+gettime1()+'\');
   stm = 'INSERT INTO `matchlog_pool` (`match_id`, `room_id`, `winner_u_id`, `entry_fee`, `max_points`, `total_winning_amt`, `winning_amount`, `deduction`, `money_type`, `created_at`,`max_player`) VALUES (NULL, '+roomnumber+', '+winner_u_id+', '+entry_fee+', '+tabletype+', '+total_winning_amt+', '+winning_amt+', '+deduction+', '+monetypeint+', \''+gettime1()+'\','+maxtype+');'
  //todo
      sqlCon.query(stm, function (err, result) {
        if (err) {return console.log(err)}//throw err;

        console.log("machlog entry added",result.insertId)
        

        for(var i= 0 ; i<playerescoppy.length;i++){
          if(playerescoppy[i].uid == winner_u_id)
          {
            stm2 = 'INSERT INTO `player_results` (`id`, `u_id`, `match_id`, `money_type`, `amount`, `created_at`, `remain_deposit`, `Remain_winning`) VALUES (NULL, '+playerescoppy[i].uid+', '+result.insertId+', '+monetypeint+', '+winning_amt+', \''+gettime1()+'\', 0, 0)'
            sqlCon.query(stm2, function (err, result){} )
            add_money_to_player(playerescoppy[i].uid,winning_amt,money_type,bonus,entry_fee)
            //deduct_money_from_player(playerescoppy[i].uid,entry_fee,money_type)
          }
          else{
            var minus = 0 - entry_fee
            stm2 = 'INSERT INTO `player_results` (`id`, `u_id`, `match_id`, `money_type`, `amount`, `created_at`, `remain_deposit`, `Remain_winning`) VALUES (NULL, '+playerescoppy[i].uid+', '+result.insertId+', '+monetypeint+', '+minus+', \''+gettime1()+'\', 0, 0)'
            sqlCon.query(stm2, function (err, result){} )
            deduct_money_from_player(playerescoppy[i].uid,entry_fee,money_type)
          }
        }
        for(var i= 0 ; i<playersleftcopy.length;i++){
          var minus = 0 - entry_fee
          stm2 = 'INSERT INTO `player_results` (`id`, `u_id`, `match_id`, `money_type`, `amount`, `created_at`, `remain_deposit`, `Remain_winning`) VALUES (NULL, '+playersleftcopy[i].uid+', '+result.insertId+', '+monetypeint+', '+minus+', \''+gettime1()+'\', 0, 0)'
          sqlCon.query(stm2, function (err, result){} )
          deduct_money_from_player(playersleftcopy[i].uid,entry_fee,money_type)
        }

      });
     
   
}

//-------------------------Onecard-----------------------------------------------------------


module.exports.insertgameresult_Onecard = function insertgameresult_Onecard(maxtype,tabletype,money_type,roomnumber,players,playersleft,winner_u_id,totalwinningpoints)
{//6 max  'paid'moneytype  0.1 tabletype
  console.log('-----------',players);
  var playerescoppy = []
  for(var i= 0 ; i<players.length;i++){
    var temp =  new Player_onecard(null,players[i].uid,players[i].username,null,maxtype,money_type,tabletype,players[i].dname,players[i].autodrop)
    temp.mypoints = players[i].mypoints
    playerescoppy.push(temp)
  }
  var playersleftcopy = []
  for(var i= 0 ; i<playersleft.length;i++){
    var temp =  new Player_onecard(null,playersleft[i].uid,playersleft[i].username,null,maxtype,money_type,tabletype,playersleft[i].dname,playersleft[i].autodrop)
    temp.mypoints = playersleft[i].mypoints
    playersleftcopy.push(temp)
  }
  var total_winning_amt = tabletype *  totalwinningpoints
  var deduction = 0
  var bonus = 0
  var monetypeint = 0
  if (money_type == 'paid'){
    monetypeint = 1
    if(maxtype == 6){
      deduction = Math.round((10/100) * total_winning_amt * 100)/100  //6 player 12 %
      bonus = Math.round((10/100) * deduction)
    }
    else{
      deduction = Math.round((10/100) * total_winning_amt * 100)/100  //2 player 12 %
      bonus = Math.round((10/100) * deduction)

    }
  }
  var winning_amt = total_winning_amt - deduction

//INSERT INTO `matchlog_points` (`match_id`, `room_id`, `winner_u_id`, `win_points`, `table_type`, `total_winning_amt`, `winning_amt`, `deduction`, `money_type`, `created_at`) VALUES (NULL, '1', '37', '60', '10', '600', '510', '90', '1', \''+gettime1()+'\');
  stm = 'INSERT INTO `matchlog_onecard` ( `match_id`, `room_id`, `winner_u_id`,`max_player`, `total_winning_amt`, `winning_amount` , `deduction` ,`max_points`, `money_type` ,`created_at`) VALUES (NULL, '+roomnumber+', '+winner_u_id+','+maxtype+',  '+total_winning_amt+', '+winning_amt+', '+deduction+','+totalwinningpoints+','+monetypeint+', \''+gettime1()+'\');'
  //todo
  sqlCon.query(stm, function (err, result) {
    if (err) {return console.log(err)}//throw err;

    console.log("machlog entry added",result.insertId)


    for(var i= 0 ; i<playerescoppy.length;i++){
      if(playerescoppy[i].uid == winner_u_id)
      {
        stm2 = 'INSERT INTO `player_results` (`id`, `u_id`, `match_id`, `money_type`, `amount`, `created_at`, `remain_deposit`, `Remain_winning`) VALUES (NULL, '+playerescoppy[i].uid+', '+result.insertId+', '+monetypeint+', '+winning_amt+', \''+gettime1()+'\', 0, 0)'
        sqlCon.query(stm2, function (err, result){})
        var amttoaddsubtract = 120 * tabletype
        add_money_to_player(playerescoppy[i].uid,winning_amt + amttoaddsubtract,money_type,bonus,amttoaddsubtract)
        //deduct_money_from_player(playerescoppy[i].uid,amttoaddsubtract,money_type)
      }
      else{
        var amt = playerescoppy[i].mypoints * tabletype
        var minus = 0 - amt
        stm2 = 'INSERT INTO `player_results` (`id`, `u_id`, `match_id`, `money_type`, `amount`, `created_at`, `remain_deposit`, `Remain_winning`) VALUES (NULL, '+playerescoppy[i].uid+', '+result.insertId+', '+monetypeint+', '+minus+', \''+gettime1()+'\', 0, 0)'
        sqlCon.query(stm2, function (err, result){} )
        deduct_money_from_player(playerescoppy[i].uid,amt,money_type)
      }
    }
    for(var i= 0 ; i<playersleftcopy.length;i++){
      var amt = playersleftcopy[i].mypoints * tabletype
      var minus = 0 - amt
      stm2 = 'INSERT INTO `player_results` (`id`, `u_id`, `match_id`, `money_type`, `amount`, `created_at`, `remain_deposit`, `Remain_winning`) VALUES (NULL, '+playersleftcopy[i].uid+', '+result.insertId+', '+monetypeint+', '+minus+', \''+gettime1()+'\', 0, 0)'
      sqlCon.query(stm2, function (err, result){} )
      deduct_money_from_player(playersleftcopy[i].uid,amt,money_type)
    }

  });


}

//------------------------------onecard end------------------------------------------------

function deduct_money_from_player(uid,amount,money_type){
  var stm = 'SELECT winning_amount,amount,coins FROM `register`  WHERE u_id = '+uid+''
  sqlCon.query(stm, function (err, result) {
    if (err) {return console.log(err)}//throw err;
    if(result.length == 0) 
    {
      return
    }
    var winBal = result[0].winning_amount
    var depoBal =  result[0].amount
    var totalbal = winBal+depoBal

    var winning_amount = 0
    var  deposite = 0
    if (money_type == 'paid'){
      if(amount > totalbal){
        deposite = totalbal - amount
        winning_amount = 0
      }
      else{
        if(amount > depoBal){
          winning_amount = totalbal - amount
          deposite = 0
        }
        else{
          winning_amount = winBal
          deposite = depoBal - amount
        }
      }
    }
    

    var stm2 = ''
    if (money_type == 'paid'){
       stm2 = 'UPDATE `register` SET  `winning_amount` = '+winning_amount+' , `amount` = '+deposite+'  WHERE u_id = '+uid+''
    }
    else{
       stm2 = 'UPDATE `register` SET  `coins` = `coins` - '+amount+'   WHERE u_id = '+uid+''
    }
    sqlCon.query(stm2, function (err, result) {});

   
  });       
}

function add_money_to_player(uid,amount,money_type,bonus,amttoaddsubtract){
 // 
    var stm2 = ''
    if (money_type == 'paid'){
       stm2 = 'UPDATE `register` SET  `winning_amount` = `winning_amount` + '+amount+' ,  `bonus` = `bonus` + '+bonus+'  WHERE u_id = '+uid+' '
    }
    else{
       stm2 = 'UPDATE `register` SET  `coins` = `coins` + '+amount+'   WHERE u_id = '+uid+''
    }
    sqlCon.query(stm2, function (err, result) {
      deduct_money_from_player(uid,amttoaddsubtract,money_type)

    });     
}

module.exports.addmoney  = function addmoneyplayer(id,value)
{
stm2 = 'UPDATE `register` SET  `winning_amount` = `winning_amount` +'+value+'   WHERE u_id = '+id+''
  sqlCon.query(stm2, function (err, result) {
if(err){
  console.log("error while adding")
}
console.log("mony added sucssesfully")
  });
}
//db.addmoney(this.players[i].uid, 100)