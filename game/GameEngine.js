const express = require('express');
const app = express();
const server = require('http').Server(app);
const io = require('socket.io')(server);
//var io = require('socket.io')(process.env.PORT || 3000)

var Player_points = require('../Classes/PlayerPoints');
var Player_papludp = require('../Classes/PlayerPapluDp');

var Player_deal = require('../Classes/PlayerDeal');
var Player_pool = require('../Classes/PlayerPool');
var Player_onecard= require('../Classes/PlayerOneCard')

pointsRoomNumber = 0
dealRoomNumber = 0
poolRoomNumber = 0
raiseRoomNumber = 0
papludpRoomNumber = 0
oneCardRoomNumber = 0

module.exports = (point, deal, pool, raise, papludp,onecard) => {
    console.log(point, deal, pool, raise, papludp,onecard)
    pointsRoomNumber = point
    dealRoomNumber = deal
    poolRoomNumber = pool
    raiseRoomNumber = raise
    papludpRoomNumber = papludp
    oneCardRoomNumber = onecard
}
var Room = require('../Classes/Room');
var RoomPoints = require('../Classes/RoomPoints');
var RoomPapluDp = require('../Classes/RoomPapluDp');

var RoomDeal = require('../Classes/RoomDeal');
var RoomPool = require('../Classes/RoomPool');
var OneCard = require('../Classes/RoomOneCard');

const db = require('../model/roomsql');


var players = [];
var rooms = []
var botnames = []
function deleteroom(room) {
    console.log("called delete room data(players):" + room.players.length)
    for (var i = 0; i < room.players.length; i++) {
        db.deleteplayerfromlive(room.players[i].username)
        // if(players[room.players[i].username].socket)
        // {
        //players[room.players[i].username].socket = null
        // }
        players[room.players[i].username] = null
        console.log("player refrence deleted main:", room.players[i].username)
        delete players[room.players[i].username]
        //console.log("player refrence deleted main:", room.players[i].username)
    }
    db.updateRoomClosed(room.number, room.gametype)
    if (rooms[room.name]) {

        rooms[room.name].stoptimers()
        rooms[room.name] = null
        delete rooms[room.name]
        console.log("room reference deleted main:", room.name)
        //console.log("remaining players:",players.length)
    }
}

module.exports.deleteroom = deleteroom
function deleteplayer(username) {
    db.deleteplayerfromlive(username)
    players[username] = null
    delete players[username]
    console.log("player refrence deleted main:", username)
}
module.exports.deleteplayer = deleteplayer





io.on('connection', function (socket) {
    console.log('connection main', socket.id)
    socket.emit('msg', { 'msg': 'you are now connected', 'bool': true });
    socket.emit('askuid', { "socket": socket.id })
    socket.emit('oldversion', {"version" : 2.0})                                        /////////// write your version number here
    socket.on('uid', function (data) {
    //console.log(data)
        if (data.socketid != socket.id) {
            console.log("mismatch")
            return
        }
       // if (data.version != 1.4) {
        //    console.log("old version")
         //   socket.emit('oldversion', {})

           // return
       // }

        console.log(data)
        var player
        var this_username
        botnames = ["ramdin verma","sharat chandran","amit","kushal","vikram singh","sanjay","abhi","ram gupta","gurmit singh","chanderpal","aman","khursid","rajeev","durgesh","nahar" , "singh","ram kumar","sunder paal","maansingh","rohit","santosh","punit","dinesh","gulshan","arvind","naushad","motilal","raj","jaswant singh","chotelal","rupesh","midda","dharam singh","ram singh","preetam kumar","sarain","pankaj","arindra","vikas","mohan singh","hemant","shivam","yash","aakash","chandesh","sumit","supriyal","pooran","irfan","azaruddin","mukul","manoj","sanjay","pawan","sandeep","parvesh","neeraj","jamil","yogita","rijul","rahul","rajender","suraj","sandeep","khala","vijaya","mayuri","sachin","Gopal","Tushar","Harshad","Akshay","Shubham","Yatish","Tony","Satyam","Shekhar","Mahesh","Aishwarya","Paro","Devdas","Vijay","Vaibhav","Gupta","Jitu","Sudarshan","Ajeet","Sujit","Amol","Bhimarao","sonya","Tatyarao","Sudeep","Raghav" ]
        if (players[data.username]) {
            console.log('Player exist in game :', players[data.username].socket.id)
            console.log(players[data.username].socket.id + " replaced by " + socket.id)
            // var oldsocket = players[data.username].socket
            players[data.username].socket = socket  //replacing socket here
            //console.log(players,sockets)
            this_username = data.username
            player = players[data.username]
            //is player inside room

            if (player.room != null) {
                //
                var room = rooms[player.room]
                room.listenOnRoom(socket, player)
                //room.deletesocket(oldsocket)

            }
            else {
                socket.emit("insufficientBal", {}); //force disconnect


            }



        }
        else {
            console.log('player not exist creating new player instanceeeeeeeeeeeeeeeeeeeeeeeeeee')
            db.inserplayerinlive(data.username, data.gametype, data.maxtype, data.moneytype, data.tabletype, data.entryfee)


            //console.log(players,sockets)
            switch (data.gametype) {
                case 'Points':
                    console.log("pointsssssssssssssssss")
                    // thismaxtype = Maxtype.getKey(1)
                    player = new Player_points(socket, data.uid, data.username, data.gametype, data.maxtype, data.moneytype, data.tabletype, data.dname, data.autodrop)

                    this_username = player.username
                    players[this_username] = player

                    //check existing room 
                    isRoomJoined = false

                    for (var i = 0; i < Object.keys(rooms).length; i++) {
                        //console.log("sdfgsdvdfsvgvefbvefbebdfb", Object.keys(rooms)[i])
                        var room = rooms[Object.keys(rooms)[i]];
                        // if(room.) conditions todo 
                        if (room.opentojoin == true && room.gametype == player.gametype && room.maxtype == player.maxtype && room.moneytype == player.moneytype && room.tabletype == player.tabletype) {

                            isRoomJoined = true
                            player.room = room.name
                            room.listenOnRoom(socket, player)
                            room.check_if_match_can_start()
                            break
                        }

                    }


                    if (isRoomJoined == false) {//creating new room
                        pointsRoomNumber++;
                        var room = new RoomPoints('R-' + pointsRoomNumber, pointsRoomNumber, io, socket, player.gametype, player, player.maxtype, player.moneytype, player.tabletype)
                        console.log("\n{ Room created: }", room.name, room.gametype, room.moneytype, room.maxtype, room.tabletype)
                        rooms['R-' + pointsRoomNumber] = room
                        player.room = 'R-' + pointsRoomNumber
                        timeout_time = setTimeout(() => {
                            if (isRoomJoined == false && room.moneytype == "free") {
                                var botrandom =  Math.floor(Math.random() * botnames.length)
                                socket.emit('botgame', { "socket": socket.id })
                                console.log("bot initiated here")
                                //room.players.length = 2
                                room.opentojoin = false
                                isRoomJoined = true
                                player = new Player_points('PcqcSeCZ1LPzx5f0AAAA', 0, 'bot', 'Points', room.maxtype, 'free', 10, botnames[botrandom] , false)

                                this_username = 'bot'
                                players[this_username] = player
                                player.room = room.name
                                room.listenOnRoom(socket, player)
                                room.check_if_match_can_start()
                                //socket.emit("bot_started");//this.io.to(this.players[0].socket.id).emit("bot_started,null);
                                //clearTimeout(timeout_time);




                            }
                        }, 5000);


                    }


                    break;
                case 'Pool':
                    player = new Player_pool(socket, data.uid, data.username, data.gametype, data.maxtype, data.moneytype, data.tabletype, data.entryfee, data.dname, data.autodrop)
                    this_username = player.username
                    players[this_username] = player

                    //check existing room 
                    isRoomJoined = false

                    for (var i = 0; i < Object.keys(rooms).length; i++) {
                        //console.log("sdfgsdvdfsvgvefbvefbebdfb", Object.keys(rooms)[i])
                        var room = rooms[Object.keys(rooms)[i]];
                        // if(room.) conditions todo 
                        if (room.opentojoin == true && room.gametype == player.gametype && room.maxtype == player.maxtype && room.moneytype == player.moneytype && room.tabletype == player.tabletype && room.entryfee == player.entryfee) {

                            isRoomJoined = true
                            player.room = room.name
                            room.listenOnRoom(socket, player)
                            room.check_if_match_can_start()
                            break
                        }

                    }


                    if (isRoomJoined == false) {//creating new room
                        poolRoomNumber++;
                        var room = new RoomPool('RPool-' + poolRoomNumber, poolRoomNumber, io, socket, player.gametype, player, player.maxtype, player.moneytype, player.tabletype, player.entryfee)
                        console.log("\n{ Room created: }", room.name, room.gametype, room.moneytype, room.maxtype, room.tabletype)
                        rooms['RPool-' + poolRoomNumber] = room
                        player.room = 'RPool-' + poolRoomNumber
                        timeout_time = setTimeout(() => {
                            if (isRoomJoined == false && room.moneytype == "free") {
                                var botrandom =  Math.floor(Math.random() * botnames.length)
                                socket.emit('botgame', { "socket": socket.id })
                                console.log("bot initiated here")
                                //room.players.length = 2
                                room.opentojoin = false
                                isRoomJoined = true
                                player = new Player_points('PcqcSeCZ1LPzx5f0AAAA', 0, 'bot', 'Pool', room.maxtype, 'free', 10, botnames[botrandom], false)

                                this_username = 'bot'
                                players[this_username] = player
                                player.room = room.name
                                room.listenOnRoom(socket, player)
                                room.check_if_match_can_start()
                                //socket.emit("bot_started");//this.io.to(this.players[0].socket.id).emit("bot_started,null);
                                //clearTimeout(timeout_time);


                            }
                        }, 5000);
                    }
                    break;
                case 'Deal':
                    player = new Player_deal(socket, data.uid, data.username, data.gametype, data.maxtype, data.moneytype, data.tabletype, data.entryfee, data.dname, data.autodrop)
                    this_username = player.username
                    players[this_username] = player

                    //check existing room 
                    isRoomJoined = false

                    for (var i = 0; i < Object.keys(rooms).length; i++) {
                        //console.log("sdfgsdvdfsvgvefbvefbebdfb", Object.keys(rooms)[i])
                        var room = rooms[Object.keys(rooms)[i]];
                        // if(room.) conditions todo 
                        if (room.opentojoin == true && room.gametype == player.gametype && room.maxtype == player.maxtype && room.moneytype == player.moneytype && room.tabletype == player.tabletype && room.entryfee == player.entryfee) {

                            isRoomJoined = true
                            player.room = room.name
                            room.listenOnRoom(socket, player)
                            room.check_if_match_can_start()
                            //console.log("smack that booty")
                            break
                        }

                    }


                    if (isRoomJoined == false) {//creating new room
                        dealRoomNumber++;
                        var room = new RoomDeal('RDeal-' + dealRoomNumber, dealRoomNumber, io, socket, player.gametype, player, player.maxtype, player.moneytype, player.tabletype, player.entryfee)
                        console.log("\n{ Room created: }", room.name, room.gametype, room.moneytype, room.maxtype, room.tabletype)
                        rooms['RDeal-' + dealRoomNumber] = room
                        player.room = 'RDeal-' + dealRoomNumber
                        timeout_time = setTimeout(() => {
                            if (isRoomJoined == false && room.moneytype == "free") {
                                var botrandom =  Math.floor(Math.random() * botnames.length)
                                socket.emit('botgame', { "socket": socket.id })
                                console.log("bot initiated here")
                                //room.players.length = 2
                                room.opentojoin = false
                                isRoomJoined = true
                                player = new Player_points('PcqcSeCZ1LPzx5f0AAAA', 0, 'bot', 'Deal', room.maxtype, 'free', 10, botnames[botrandom], false)

                                this_username = 'bot'
                                players[this_username] = player
                                player.room = room.name
                                room.listenOnRoom(socket, player)
                                room.check_if_match_can_start()
                                //socket.emit("bot_started");//this.io.to(this.players[0].socket.id).emit("bot_started,null);
                                //clearTimeout(timeout_time);




                            }
                        }, 5000);
                    }
                    break;
                case 'Raise':
                    break;
                case 'PapluDp':
                    // thismaxtype = Maxtype.getKey(1)
                    player = new Player_papludp(socket, data.uid, data.username, data.gametype, data.maxtype, data.moneytype, data.tabletype, data.dname, data.autodrop)
                    this_username = player.username
                    players[this_username] = player

                    //check existing room 
                    isRoomJoined = false

                    for (var i = 0; i < Object.keys(rooms).length; i++) {
                        //console.log("sdfgsdvdfsvgvefbvefbebdfb", Object.keys(rooms)[i])
                        var room = rooms[Object.keys(rooms)[i]];
                        // if(room.) conditions todo 
                        if (room.opentojoin == true && room.gametype == player.gametype && room.maxtype == player.maxtype && room.moneytype == player.moneytype && room.tabletype == player.tabletype) {

                            isRoomJoined = true
                            player.room = room.name
                            room.listenOnRoom(socket, player)
                            room.check_if_match_can_start()
                            break
                        }

                    }


                    if (isRoomJoined == false) {//creating new room
                        papludpRoomNumber++;
                        var room = new RoomPapluDp('RPaplu-' + papludpRoomNumber, papludpRoomNumber, io, socket, player.gametype, player, player.maxtype, player.moneytype, player.tabletype)
                        console.log("\n{ Room created: }", room.name, room.gametype, room.moneytype, room.maxtype, room.tabletype)
                        rooms['RPaplu-' + papludpRoomNumber] = room
                        player.room = 'RPaplu-' + papludpRoomNumber
                        timeout_time = setTimeout(() => {
                            if (isRoomJoined == false && room.moneytype == "free") {
                                var botrandom =  Math.floor(Math.random() * botnames.length)
                                socket.emit('botgame', { "socket": socket.id })
                                console.log("bot initiated here")
                                //room.players.length = 2
                                room.opentojoin = false
                                isRoomJoined = true
                                player = new Player_points('PcqcSeCZ1LPzx5f0AAAA', 0, 'bot', 'PapluDp', room.maxtype, 'free', 10, botnames[botrandom], false)
                                this_username = 'bot'
                                players[this_username] = player
                                player.room = room.name
                                room.listenOnRoom(socket, player)
                                room.check_if_match_can_start()
                                //socket.emit("bot_started");//this.io.to(this.players[0].socket.id).emit("bot_started,null);
                                //clearTimeout(timeout_time);




                            }
                        }, 5000);
                    }
                    break;

                    //one card game code

                case 'OneCard':
                    console.log('enter in onecard game');
                    player = new Player_onecard(socket, data.uid, data.username, data.gametype, data.maxtype, data.moneytype, data.tabletype, data.entryfee, data.dname, data.autodrop)
                    this_username = player.username
                    players[this_username] = player
                    isRoomJoined = false

                    for (var i = 0; i < Object.keys(rooms).length; i++) {
                        var room = rooms[Object.keys(rooms)[i]];
                        if (room.opentojoin == true && room.gametype == player.gametype && room.maxtype == player.maxtype && room.moneytype == player.moneytype && room.tabletype == player.tabletype && room.entryfee == player.entryfee) {
                            isRoomJoined = true
                            player.room = room.name
                            room.listenOnRoom(socket, player)
                            room.check_if_match_can_start()
                            break
                        }

                    }
                    if (isRoomJoined == false) {//creating new room
                        oneCardRoomNumber++;
                        console.log(oneCardRoomNumber);
                        var room = new OneCard('ROneCrad-' + oneCardRoomNumber, oneCardRoomNumber, io, socket, player.gametype, player, player.maxtype, player.moneytype, player.tabletype, player.entryfee)
                        console.log("\n{ Room created: }", room.name, room.gametype, room.moneytype, room.maxtype, room.tabletype)
                        rooms['ROneCard-' + oneCardRoomNumber] = room
                        player.room = 'ROneCard-' + oneCardRoomNumber
                        console.log("i am in game-engine");
                         timeout_time = setTimeout(() => {
                            if (isRoomJoined == false && room.moneytype == "free") {
                                player.room = room.name
                                room.listenOnRoom(socket, player)
                                room.check_if_match_can_start()
                            }
                         }, 5000);

                    }
                    break;

            }


        }


        socket.emit('msg', { 'msg': 'you socket id is' + socket.id });



    })

    //socket.emit('askuid','askuidandtype');  

    //check if player exist by uid
    //if exist replace socket id
    //else create new player

    //get room list with status waiiting for player
    //join room
    //else create new room join player 




    //const room = new Room('room1'+socket.id,io , socket , player)
    //todo if reconnected with different socket id, 
    //we have uid


    socket.on('exit', (data) => {
        console.log(data.username + ':Did Exit from main:' + socket.id)
        db.deleteplayerfromlive(data.username)
        delete players[data.username]

    });
    socket.on('disconnect', function () {
        console.log('disconnected main', socket.id + ' ,IP: ' + socket.handshake.address)

        // var player = this.players.find(x => x.socket === socket);
        // console.log("found player"+player)
        // var room = player.room
        // console.log('found room :'+rooms[room])

    });





})
server.listen(process.env.PORT || 3002);






/*
  switch (data.gametype){
                    case 'Points':
                      isRoomJoined = false
                        for(var i = 0 ;i < Object.keys(rooms).length ;i++){
                            //console.log("sdfgsdvdfsvgvefbvefbebdfb", Object.keys(rooms)[i])
                            var room = rooms[Object.keys(rooms)[i]];
                            if(room.opentojoin  == true  && room.maxtype == player.maxtype && room.moneytype == player.moneytype && room.tabletype == player.tabletype)
                            {
                              isRoomJoined = true
                            player.room = room.name
                            room.listenOnRoom(socket,player)
                            break
                            }
                          
                        }
                    
                          if(isRoomJoined == false){
                            pointsRoomNumber++
                            var room = new RoomPoints('R-'+pointsRoomNumber,pointsRoomNumber,io,socket,player.gametype,player,player.maxtype,player.moneytype,player.tabletype)
                            console.log("\n{Room created:}",room.name,room.gametype,room.moneytype,room.maxtype,room.tabletype)
                            rooms['R-'+pointsRoomNumber] = room
                            player.room = 'R-'+pointsRoomNumber
                          }

                        break;
                      case 'Deal':
                        break;
                }

*/