      const main = require('../game/GameEngine')
      const db = require('../model/roomsql');
      //const pointsscript = require('./RoomPoints')
      //var Player_points = require('../Classes/RoomPoints');
      const Enum = require('enum');
      const Status = new Enum(['waiting', 'standby','reseat', 'started'])
      const Card = require('./Card')
      

      

      class Room {
      
        //constructor will be called after we create my sql entry of the room 
          constructor(name,number, io , socket,gametype , player , maxtype,moneytype) {
            this.name = name
            this.number = number
            //this.players = [player];
            
            this.io = io
            //this.socket = socket
            //this.usernames = []
            this.opentojoin = true
            this.gametype =  gametype

            this.maxtype =  maxtype
            this.moneytype =  moneytype

            this.sockets = []
            this.players = []
            

            this.jokercard = null 
            this.papluJoker = null    
            this.pickablecard = null  // sent all time
            this.discardedcards = []  // sent all time

            this.deckcards = []  // will  at serverside

            this.pickedcard = null;

            this.timer_closeroom = null
            this.alldisconnected = false 

            this.isthisfirstround = true

            this.playersleft = []

            this.areSeatsRanked = false

            this.matchtimeoutObj = null
            this.matchtimeoutObjStarttime = null
            this.reseattimeoutObj = null

            this.timer_nextturn = null
            this.timer_claimwinwait = null
            this.timer_clamwin = null

            this.restartmatchtimeoutObj = null

            this.matchstatus = Status.waiting

            this.user_who_claimed_win = ""

            this.wintype = 0

            this.timeamt = 30
            this.turncounter = 1 
	    this.a = 0 
           
            //max player 2/6
            //table type  0rs, 0.1 rs, 1 rs, 10 rs, 100 rs 
            //game status waiting ingame 
            // room id 
            // this.max players
            //this.listenOnRoom(socket,players);
          }
          resetvalues(){
            this.stoptimers()
            
            this.jokercard = null  
            this.papluJoker = null    
   
            this.pickablecard = null  // sent all time
            this.discardedcards = []  // sent all time

            this.deckcards = []  // will  at serverside

            this.pickedcard = null;


            this.timer_closeroom = null

            this.isthisfirstround = true

            this.matchtimeoutObj = null
            this.matchtimeoutObjStarttime = null
            this.reseattimeoutObj = null

            this.timer_nextturn = null
            this.timer_clamwin = null
            this.timer_claimwinwait = null

            this.matchstatus = Status.waiting

            for(var i= 0 ; i<this.players.length;i++){
              
              this.players[i].resetvalues()
            }
            this.turncounter = 1 
          }
          
          check_insufficient_balance(expectedAmt){
            if(this.moneytype == 'free'){
              return
            }
            console.log('expected amt is:',expectedAmt)
            for(var i= 0 ; i<this.players.length;i++){
              var that = this
              
            db.hasEnoughBalance(this.players[i].uid,expectedAmt,this.players[i].socket.id,this.moneytype,function(hasenough,socketid ){
               if(hasenough==false)
               {
                that.io.to(socketid).emit("insufficientBal", {})
                console.log("sending insufficient bal to",socketid)
                
               }
             })

            }
          }
          
          broadcastroom(msg , data){
          
          for(var i= 0 ; i<this.players.length;i++){
            //console.log("sending:",msg, data, ' to:',this.players[i].username)
	    if (this.players[i].username != 'bot') {
            this.io.to(this.players[i].socket.id).emit(msg, data);
          }
	    
			
	}
        }
          broadcastroom_except_me(msg , data, username)
          {
            for(var i= 0 ; i<this.players.length;i++){
              if(username != this.players[i].username  && this.players[i].username != 'bot')
              {
                //console.log("sending:",msg, data, ' to:',this.players[i].username)

              this.io.to(this.players[i].socket.id).emit(msg, data);
              }
            }
          }
          getTimeLeft(timeout , startdate) {
      
            return  Math.ceil(( startdate - Date.now() + timeout._idleTimeout) /1000 )
        }
      
          deletesocket(socket){
            const index = this.sockets.indexOf(socket.id);
            if (index > -1) {
              this.sockets.splice(index, 1);
              }
          }
          
          
          getplayerifpresent(player){
            const index = this.players.indexOf(player);
            if (index > -1) {
              return this.players[index]
            }
            else{
              return null
            }
          }
          addplayerto_playerleft_onexit(socket){
            var player =this.players.find(x => x.socket === socket);
            if(!player){
              return
            }
            this.playersleft.push(player)
          }
          deleteplayeronexit(socket)
          {
            var player =this.players.find(x => x.socket === socket);
            // lodash.filter(this.players, x => x.socket === socket);
            if(!player){
              return
            }
            const index = this.players.indexOf(player);
            this.players[index].isdropped = true
            if(this.isthisfirstround == true){
              player.mypoints = 20
            }
            else{
              player.mypoints = 40
            }
            console.log(player.username,":Did exit: deleting from [players]")
            this.deleteplayer_from_players(player)

            console.log(this.players.length+":players Remain")
            //broadacast player left
          }
          deleteplayer_from_players(player)
          {
            const index = this.players.indexOf(player);
            if (index > -1) {
              this.players.splice(index, 1);

            }
          }
          updateplayerpoints(data)
          {
	   var player =this.players.find(x => x.username === data.username);
            if(player){
              console.log('updating player point and cards')
              if(player.isdropped == false){

                player.mypoints = data.mypoints<80?data.mypoints:80
              }
              player.mycards.splice(0, player.mycards.length)
              for(var i= 0 ; i<data.cards.length;i++){
                var temp = Card.newcard(data.cards[i].number,data.cards[i].color,data.cards[i].isjoker)
                player.mycards.push(temp)
              }
            }
          }
          dropplayer(username ,byinvalidclaim){
            var player =this.players.find(x => x.username === username);
            if(player){
              player.isdropped = true
              player.mycardsshown = true
              
              if(byinvalidclaim == true){
                player.mypoints = 80
              }
              else{
                if(this.isthisfirstround == true){
                  player.mypoints = 20
  
                }
                else{
                  player.mypoints = 40
                }
              }
              var nondropcount = this.players.length
              for(var i= 0 ; i<this.players.length;i++){
                if(this.players[i].isdropped == true)
                {
                  nondropcount--
                }
              }
              console.log("non drop remains:",nondropcount)
              if(nondropcount <= 1){
                //todo set last player as winner
                for(var i= 0 ; i<this.players.length;i++){
                  if(this.players[i].isdropped == false)
                  {
                    this.players[i].amiwon = true 
                    if(this.players[i].username != 'bot')
                    {
                    if(this.gametype == "PapluDp") {
                      this.wintype =  this.players[i].get_no_of_paplu_joker(this.jokercard.number,this.jokercard.color)
                    }
                  }
                  
                }
                }
                console.log("paplu")
                this.stopgame(false)
                console.log("paplustopped")
              }
              else{
                this.nextturn_waitend()
              }
            }
          }
          get_dropped_count(){
            var dropcount = 0
            for(var i= 0 ; i<this.players.length;i++){
              if(this.players[i].isdropped == true)
              {
              dropcount++
              }
            }
            return  dropcount
          }
          get_lastNondrop_index(){
            for(var i= 0 ; i<this.players.length;i++){
              if(this.players[i].isdropped == false)
              {
                return  i
              }
            }
            return 0
           
          }
          

          updateplayer_shown_status(username)
          {
            var player =this.players.find(x => x.username === username);
            if(player){
              console.log('updating player mycardsshown')
              player.mycardsshown = true

            }
          
          }
          update_set_me_won(username){
            var player =this.players.find(x => x.username === username);
            if(player){
              console.log('updating player win status',player.username)
              player.amiwon = true
            }
          }
          checkwheatherallcardsshown(){
            for(var i= 0 ; i<this.players.length;i++){
              if(this.players[i].mycardsshown == false )
              {
                return false
              }

            }
            return true
          }
          

          listenOnRoom(socket ,player) {
            this.alldisconnected = false
            //clearTimeout(this.timer_closeroom) //

              this.sockets.push(socket.id)
              var tempplayer = this.getplayerifpresent(player)
              if(tempplayer == null){
                this.players.push(player)
                console.log("this is new player",player.username)
              }else{
                console.log("this is EXISTING player",player.username)
                console.log("socket:",player.socket.id,tempplayer.socket.id)
                player.isdisconnected = false
                /*var timeremain  = pointsscript.getremaintime
                console.log("time:" + timeremain)*/

              }

              socket.on('autodrop', (msg) => {
                //console.log(msg+':msg:'+socket.id)
                var player =this.players.find(x => x.socket.id === socket.id);
                if(player)
                {
                  player.autodrop = msg.autodrop
                  //console.log(this.players,"player.autodrop",player.username)
                }
               });
              socket.on('msg', (msg) => {
                      console.log(msg+':msg:'+socket.id)
                      if(msg.name == 'b'){
                        this.broadcastroom('msg' , {"msg":"broadcasting "})
                      }

              });
              socket.on('exit', (data) => {
                //
                // if(this.matchstatus == Status.started){
                //   console.log('player leaving while match started');
                //   if(data.isthismyturn == true){
                //     this.nextturn_waitend()
                //   }
                  
                //   this.addplayerto_playerleft_onexit(socket)
                //       //if match already started and it is  my turn
                      

                // }
                // console.log(data.username,':Exit from room:',socket.id)

                // this.deleteplayeronexit(socket)
                // this.deletesocket(socket)
                // main.deleteplayer(data.username)
                // if( this.players.length == 0 )  //this.users.length == 0 ||
                // {
                //   console.log('closed room onexit:',this.name)
                //   //if game is started closemthe game first
                //   main.deleteroom(this)
                //   //todo entry in database
                // }
                
                // if( this.players.length == 1 && this.matchstatus == Status.started){
                //   this.players[0].amiwon = true
                //   this.stopgame(true)

                // }
                
                  
                });

            socket.on('disconnect', (msg) => {
              var player = this.players.find(x => x.socket === socket)
              //player is undefined in case of already exit
              if(player)
              {
                player.isdisconnected = true
              }
              else
              {
                console.log('player already exited,now disconnecting',socket.id)
              }
              console.log('disconnected inside room',socket.id ,' ,IP: ', socket.handshake.address)
              this.deletesocket(socket)
                if(this.sockets.length == 0)
                {
                  //todo if match is runnuning  close match 

                  this.alldisconnected = true
                  this.closeroom_starts() 


                }
                
              });
            
              
          }
          closeroom_starts() 
          {
            clearTimeout(this.timer_closeroom)
            this.timer_closeroom = setTimeout(() => {
              if(this.alldisconnected == true){
                //to do if match
                  //all disconnected so match is tie 
                  main.deleteroom(this)
                  console.log('closed room ondisconnect after delay:',this.name)          
              }

            },60000)
          }
          maindeleteplayer(username){
            main.deleteplayer(username)
          }

          reseat_player_sort(){
            //console.log("before sort",this.players)
            
            for(var i = 0 ; i<this.players.length - 1 ;i++){
              if(i==0){
                for(var j = i + 1 ; j < this.players.length  ;j++)
                {
  
                  var first = this.players[i].reseatcard.number
                  var second = this.players[j].reseatcard.number
                  first = first == 0 ? 13 : first
                  second = second == 0 ? 13 : second
                  if (first < second){
  
                    var temp = this.players[i]; 
                    this.players[i] = this.players[j]; 
                    this.players[j] = temp; 
                  }
                  else if(first == second){
                    var firstC = this.players[i].reseatcard.color
                    var secondC = this.players[j].reseatcard.color
                    if (firstC > secondC){
                      var temp = this.players[i]; 
                      this.players[i] = this.players[j]; 
                      this.players[j] = temp;
                    }
                  }
                }
              }
              else{
                for(var j = i + 1 ; j < this.players.length  ;j++)
                {
  
                  var first = this.players[i].reseatcard.number
                  var second = this.players[j].reseatcard.number
                  first = first == 0 ? 13 : first
                  second = second == 0 ? 13 : second
                  if (first > second){
  
                    var temp = this.players[i]; 
                    this.players[i] = this.players[j]; 
                    this.players[j] = temp; 
                  }
                  else if(first == second){
                    var firstC = this.players[i].reseatcard.color
                    var secondC = this.players[j].reseatcard.color
                    if (firstC < secondC){
                      var temp = this.players[i]; 
                      this.players[i] = this.players[j]; 
                      this.players[j] = temp;
                    }
                  }
                }
              }
              
            }
          
            //console.log("after sort",this.players)
          }
          

          
      }
      module.exports = Room
      module.exports.Status = Status

    //   module.exports.getTimeLeft = function getTimeLeft(timeout , startdate) {
    //     return  Math.ceil(( startdate - Date.now() + timeout._idleTimeout) /1000 )
    // }