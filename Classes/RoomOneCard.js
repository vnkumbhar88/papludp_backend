const Room = require('./Room')
const db = require('../model/roomsql');
const Card = require('./OneCard')
const main = require('../game/GameEngine')
const Status = Room.Status

module.exports = class RoomOneCard extends Room
{

    constructor(name,number, io , socket,gametype , player , maxtype,moneytype ,tabletype)
    {
        console.log("name=>sguhj",name);
        super(name,number, io , socket,gametype , player , maxtype,moneytype)
        this.tabletype = tabletype
        //entry in database , register match id
        this.listenOnRoom(socket,player);
        console.log("onecard listenOnRoom")

        db.insertOneCardRoom(number,maxtype,moneytype,this.tabletype,function(err,result){
            console.log("in RoomOnecard", number);
            if (err != null) return;
            console.log('Room added in database:'+result.insertId+":database id" , name+'room id')
            // if(result.insertId!=name)
        })

    }
    resetvalues()
    {
        super.resetvalues()
        this.wintype = 0
        this.opentojoin = true
        this.playersleft = []  //only in room points

    }


    listenOnRoom(socket ,player)
    {
        super.listenOnRoom(socket,player)
        setTimeout(() => {
            this.broadcastroom('playerjoined' , this.get_players_data())
        }, 500);
        this.check_insufficient_balance(80*this.tabletype)
        console.log(player.gametype,'room no:'+this.name+' joined by:'+player.username,socket.id)
        socket.emit('matchstatus' , {'matchstatus':this.matchstatus.key})
        socket.emit('mystatus' , {"isdropped":player.isdropped,"joker":this.jokercard,"hidden":this.pickablecard,"discarded":this.discardedcards,"cards":player.mycards,"papluJoker":this.papluJoker})

        socket.on('drop', (data) => {
            console.log("player dropping",data.username)
            this.dropplayer(data.username,false)
            this.broadcastroom('playerjoined' , this.get_players_data())  //to refresh dropped player

        });
        socket.on('drop_by_invalid_claim', (data) => {
            clearTimeout(this.timer_claimwinwait)
            console.log("player dropping",data.username)
            this.dropplayer(data.username,true)
            this.broadcastroom('playerjoined' , this.get_players_data())  //to refresh dropped player

        });

        socket.on('exit', (data) => {
            if(this.matchstatus == Status.started){
                if(data.isthismyturn == true){
                    this.nextturn_waitend()
                }
                this.addplayerto_playerleft_onexit(socket)
                //if match already started and it is  my turn
            }


            this.deleteplayeronexit(socket)
            this.deletesocket(socket)
            main.deleteplayer(data.username)

            if (this.players.length == 1 && this.matchstatus == Status.started) {
                this.players[0].amiwon = true
                   console.log("stoppedotherway")
                    this.stopgame_otherway();
            }
            if( this.players.length == 0 )
            {
                main.deleteroom(this)
            }

            if( this.players.length - this.get_dropped_count() == 1 && this.matchstatus == Status.started){
                this.players[this.get_lastNondrop_index()].amiwon = true
                this.stopgame(true)

            }

            this.broadcastroom('playerleft' , this.get_players_data())
            if(this.matchstatus == Status.waiting  || this.matchstatus == Status.standby){
                this.opentojoin = true;
                if(this.players.length == 1){

                        console.log('closed room onexit:', this.name)
                        main.deleteplayer('bot')
                        main.deleteroom(this)
                   // }
                }
            }

            if(this.matchstatus == Status.reseat){
                clearTimeout(this.reseattimeoutObj)
                if(this.players.length < 2){
                        console.log('closed room onexit:', this.name)
                        //if game is started closemthe game first
                        main.deleteplayer('bot')
                        main.deleteroom(this)
                        //todo entry in database
                  //  }
                }

                else{
                    this.waittimer_reseat()
                }

            }
            if(this.matchstatus == Status.started){
                console.log('left while match started: roompoints')
            }

            socket.disconnect()
        });


        this.broadcastroom('playerjoined' , this.get_players_data())

        //todo  forse exit player in case of low balance

    }
    get_players_data(){
        var temparray = [];
        for (var i = 0; i < this.players.length; i++) {
            console.log('user data');
            temparray.push({'username': this.players[i].username, 'uid': this.players[i].uid ,'ismyturn':this.players[i].ismyturn ,'isdropped':this.players[i].isdropped ,'reseatcard':this.players[i].reseatcard,'dname':this.players[i].dname,'amilast':this.players[i].amilast,'buyin':this.players[i].buyin});
        }
        return {"all":temparray}
    }
    get_players_results(){
        var temparray = [];
        for (var i = 0; i < this.players.length; i++) {

            temparray.push({'username': this.players[i].username, 'uid': this.players[i].uid  ,'isdropped':this.players[i].isdropped ,'cards':this.players[i].reseatcard , 'points':this.players[i].mypoints ,'isshown':this.players[i].mycardsshown,'amiwinner':this.players[i].amiwon,'dname':this.players[i].dname});

        }
        for (var i = 0; i < this.playersleft.length; i++) {

            temparray.push({'username': this.playersleft[i].username, 'uid': this.playersleft[i].uid  ,'isdropped':this.playersleft[i].isdropped ,'cards':this.playersleft[i].reseatcard , 'points':this.playersleft[i].mypoints ,'isshown':this.playersleft[i].mycardsshown,'amiwinner':false,'dname':this.playersleft[i].dname});

        }
        return {"all":temparray}
    }

    check_if_match_can_start(){

        //console.log("status.waiting" + Status.waiting)
        if(this.matchstatus == Status.waiting){
            this.standby_match()
        }
        else if(this.matchstatus == Status.standby){
            //todo send msg to myself
            const t = this.getTimeLeft(this.matchtimeoutObj,this.matchtimeoutObjStarttime)
            console.log('Time left: '+t+'s');
            this.broadcastroom('wait' , { "time":t,'room':this.name})
            this.broadcastroom('matchstatus' , {'matchstatus':this.matchstatus.key})

        }
        if(this.maxtype <= this.players.length)
        {
            this.opentojoin  = false ;
        }
    }
    standby_match(){
        if( this.players.length > 1){
            this.matchstatus = Status.standby
            //this.matchstarted = true
            this.broadcastroom('wait' , { "time":10})
            this.broadcastroom('matchstatus' , {'matchstatus':this.matchstatus.key})
            clearTimeout(this.matchtimeoutObj)
            clearTimeout(this.reseattimeoutObj) //not needed otptional
            this.waittimer_startMatch(10000)

        }
    }
    waittimer_startMatch(sec) {
        console.log("here")
        this.matchtimeoutObjStarttime = Date.now()
        this.matchtimeoutObj = setTimeout(() => {

            for (var i = 0; i < this.players.length; i++) {
                if(this.players[i].isdisconnected == true){
                    this.maindeleteplayer(this.players[i].username)
                    this.players.splice(i, 1);
                    i--;
                    this.broadcastroom('playerleft' , this.get_players_data())

                }

            }
            if( this.players.length < 2)
            {
                this.broadcastroom('msg' , {"msg":'waiting for opponenets'})
                this.matchstatus = Status.waiting
                this.opentojoin = true
                this.broadcastroom('matchstatus' , {'matchstatus':this.matchstatus.key})
                return
            }

            //this.matchstarted = true
            console.log('timeout beyond time, match  started')
            this.broadcastroom('msg' , {"msg":'start','room':this.name})

            if(this.areSeatsRanked == false){

                //this.areSeatsRanked = true
                ////shuffle ranks
                this.waittimer_reseat()

            }
            else{
                //shuffle cards
                //send first turn
                this.shufflecards()


            }

        }, sec);

    }

    waittimer_reseat()
    {

        this.matchstatus = Status.reseat
        this.broadcastroom('matchstatus' , {'matchstatus':this.matchstatus.key})
        this.broadcastroom('RoomId' , {"time":5 ,'room':this.name})

        this.matchtimeoutObj = setTimeout(() => {
            //reseat players here according to
            if( this.players.length < 2)
            {
                this.broadcastroom('msg' , {"msg":'waiting for opponenets'})
                this.matchstatus = Status.waiting
                this.opentojoin = true
                this.broadcastroom('matchstatus' , {'matchstatus':this.matchstatus.key})
                return
            }

            this.reseat_player_sort()
            this.broadcastroom('playerjoined' , this.get_players_data())  //to refresh dropped player
            console.log('xxreseat timeout')
            this.players[0].ismyturn = true;
            this.players[0].amiwon = true;
            this.stopgame(true);
            this.resetvalues();
        },3000)
    }
    stoptimers(){

        if (this.reseattimeoutObj) {
            clearInterval(this.reseattimeoutObj);
            this.reseattimeoutObj = null;
        }
    }

    stopgame(should_stop_fullgame){
        console.log('stopping the game');
        //update database
        //console.log("playyyyyyyyyyyyyyyyyers length" + this.players.length)
        this.update_winner_in_database(should_stop_fullgame)
        this.resetvalues()
        //broadcast results

        this.restartmatch()
    }
    restartmatch(){
        clearInterval(this.restartmatchtimeoutObj)
        this.restartmatchtimeoutObj = setTimeout(() => {
            console.log('reseat timeout')
            this.check_insufficient_balance(80*this.tabletype)

            this.matchstatus = Status.waiting
            this.check_if_match_can_start()
            this.broadcastroom('playerjoined' , this.get_players_data())  //to refresh dropped player


        },3000)
    }
    update_winner_in_database(should_stop_fullgame)
    {
        var winnerindex = 0
         var totalwinningpoints = 0
        var holdingmoneyback = this.tabletype * 80
        for(var i= 0 ; i<this.players.length;i++)
        {
            if(this.moneytype == "paid")
            {
                db.addmoney(this.players[i].uid, holdingmoneyback)
            }

            if(this.players[i].amiwon == true && winnerindex == 0)
            {
                winnerindex = i
                this.players[i].mypoints = 0
            }
            else{
                if(this.players[i].mypoints>80){
                    this.players[i].mypoints = 80
                }
                if(this.players[i].mypoints == 0){
                    this.players[i].mypoints = 2
                }
                this.players[i].update_buyin(0-this.players[i].mypoints )
                totalwinningpoints = totalwinningpoints + this.players[i].mypoints
                console.log('player:',this.players[i].username," lost ",this.players[i].mypoints,' points')

            }
            console.log("hhh")

        }

        for(var i= 0 ; i<this.playersleft.length;i++)
        {
            console.log("xxreseat")
            if(this.playersleft[i].username == 'bot')
            {
                this.playersleft[i].mypoints   =Math.floor(Math.random() * Math.floor(70))
                console.log("herepoints" +this.playersleft[i].mypoints )
                //this.playersleft[i].mypoints = 60
            }

            if(this.playersleft[i].mypoints>80){
                this.playersleft[i].mypoints = 80
            }
            if(this.playersleft[i].mypoints == 0){
                this.playersleft[i].mypoints = 2
            }
            if(this.moneytype == "paid")
            {
                db.addmoney(this.playersleft[i].uid, holdingmoneyback)
            }
            totalwinningpoints = totalwinningpoints + this.playersleft[i].mypoints
            console.log('player:',this.playersleft[i].username,"lost",this.playersleft[i].mypoints,'points')
            //todo update loser in database

        }

            var deduction = 0
         if (this.moneytype == 'paid'){
          if(this.maxtype == 6){
                deduction = Math.round((10/100) * totalwinningpoints * 100)/100 //6 player 12 %
            }
            else{
                deduction = Math.round((10/100) * totalwinningpoints *100)/100  //2 player 12 %

            }
        }
        if(winnerindex == 0){
            console.log('winneris:',this.players[winnerindex].username)
            console.log('and winning points are :',totalwinningpoints,deduction)
            //todo update winner database
            this.players[winnerindex].update_buyin(totalwinningpoints - deduction + 50)
        }
          var winning_amt = 50//totalwinningpoints - deduction
          totalwinningpoints = totalwinningpoints + 1000
          db.addmoney(this.maxtype,this.tabletype,this.moneytype,this.number,this.players,this.playersleft,this.players[winnerindex].uid,totalwinningpoints)
          this.broadcastroom('resultOneCard',{"players":this.get_players_results(),"joker":this.jokercard,"totalwinningpoints":winning_amt})
          console.log("max"+ this.maxtype + "     table "+this.tabletype + "       money:" + this.moneytype +"     number" + this.number + "         players:" +this.players[0].username + "       left" + this.playersleft +"       playuersss:" + this.players[winnerindex].uid+ "       amt"+totalwinningpoints)
          db.insertgameresult_Onecard(this.maxtype,this.tabletype,this.moneytype,this.number,this.players,this.playersleft,this.players[winnerindex].uid,totalwinningpoints,this.wintype)
          db.addmoney(this.players[0].uid, 50)

    }

    stopgame_otherway(){
        this.dealcount = 0
        this.poolstarted= false
        this.broadcastroom('winner',this.get_onecard_winner())
        db.insertgameresult_Onecard(this.maxtype,this.tabletype,this.moneytype,this.number,this.players,this.playersleft)
        this.resetvalues()
        this.matchstatus = Status.waiting;
        this.broadcastroom('matchstatus' , {'matchstatus':this.matchstatus.key})

    }

    get_onecard_winner(){
        var leadinguid = 0
        for(var i= 1 ; i<this.players.length;i++){
            var firsttotal = this.players[leadinguid].gettotalpoints()
            var thistotal = this.players[i].gettotalpoints()
            if(thistotal < firsttotal){
                leadinguid = i
            }
        }
        var leadinguid =  this.players[leadinguid].username
        console.log('***leadinguid***',leadinguid);
        return {'username':[leadinguid]}
    }
}

function getTimeLeft(timevar) {
    return Math.ceil((timevar._idleStart + timevar._idleTimeout) / 1000 - process.uptime());
}