        var shortID = require('shortid')
        //const users = require('../model/users')
        const Card = require('./Card')
        const con = require('../sqlconfig');


        class Player{

        constructor(socket,uid ,username,gametype  ,maxtype,moneytype,dname ,autodrop){

        this.username =  username//users.getUser(u_id)
        this.dname = dname

        // this.id =  shortID.generate()
        this.balance = 0
        this.uid = uid
        this.socket = socket
        this.gametype = gametype //point,...
        this.maxtype = maxtype //2 player
        this.moneytype = moneytype //free paid
        this.room = null
        this.isdisconnected = false

        this.mycardsshown = false
        this.mypoints = 0

        this.mycards = []

        this.ismyturn = false
        this.isdropped = false  
        this.isexited = false
        this.amiwon = false

        this.reseatcard = Card.get_random_card()

        this.amilast = false

        this.turnMissed = 0
        this.autodrop = autodrop;
        this.buyin  = 240
        }
        setroom(room){
        this.room = room
        }
        resetvalues(){
        this.mycardsshown = false
        this.mypoints = 0
        this.mycards = []
        this.ismyturn = false
        this.isdropped = false 
        this.amiwon = false
        this.turnMissed = 0
        this.reseatcard = Card.get_random_card()


        }

        get_time_amt(){
        if(this.turnMissed == 0 ){
        return 25
        }
        else if(this.turnMissed == 1 ){
        return 20
        }
        else if(this.turnMissed == 2 ){
        return 15
        }
        else if(this.turnMissed == 3 ){
        return 10
        }
        else {
        return 10
        }
        }
        increment_turn_missed(){
        this.turnMissed ++
        }
        reset_turn_missed(){
        this.turnMissed = 0
        }
        get_turn_missed(){
        return this.turnMissed
        }

        assign_buyin(){
        console.log("assing buy in Player")

        }


        // samplefunction(){
        //     console.log('sample function called from player')
        // }
        // getusername2(){
        //     users.getUser(this.u_id,(r)=> {
        //         console.log("databas value",r.name)
        //         this.username = r.name

        //     })


        // }

        //todo replace player socket  id  in case of internet disconnection


        }
        module.exports = Player