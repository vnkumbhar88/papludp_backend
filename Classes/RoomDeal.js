const Room = require('./Room')
const db = require('../model/roomsql');
const Card = require('./Card')
//const Enum = require('enum');
const main = require('../game/GameEngine')


const Status = Room.Status//new Enum(['waiting', 'standby','reseat', 'started'])
var startTimeMS = null
var whoseturn = null



module.exports = class RoomDeal extends Room
{


    constructor(name,number, io , socket,gametype , player , maxtype,moneytype ,tabletype,entryfee) 
    {
        super(name,number, io , socket,gametype , player , maxtype,moneytype)
        this.tabletype = tabletype
        this.entryfee = entryfee  //25 rs
        //entry in database , register match id
        this.listenOnRoom(socket,player);
        this.remain_deals = tabletype

        
        db.insertDealRoom(number,maxtype,moneytype,this.tabletype,entryfee,function(err,result){
          if (err != null) return;
          console.log('Room added in database:'+result.insertId+":database id" , name+'room id')
          // if(result.insertId!=name)
        })

      }
      resetvalues(){
        super.resetvalues()
        
        if(this.remain_deals == 0){
          this.areSeatsRanked = false
          this.opentojoin = true
          this.remain_deals = this.tabletype
          this.playersleft = []  //only in room points
          for(var i= 0 ; i<this.players.length;i++){
              
            this.players[i].resetplayerdealvalues()
          }

        }


      }
    

      listenOnRoom(socket ,player) 
      {
        super.listenOnRoom(socket,player)
        setTimeout(() => {
          this.broadcastroom('playerjoined' , this.get_players_data())
        }, 500);
        this.check_insufficient_balance(this.entryfee)
        console.log(player.gametype,'room no:'+this.name+' joined by:'+player.username,socket.id)
        socket.emit('matchstatus' , {'matchstatus':this.matchstatus.key})
        socket.emit('mystatus' , {"isdropped":player.isdropped,"joker":this.jokercard,"hidden":this.pickablecard,"discarded":this.discardedcards,"cards":player.mycards,"papluJoker":this.papluJoker})

        
        
        socket.on('drop', (data) => {
          console.log("player dropping",data.username)
          this.dropplayer(data.username,false)
          this.broadcastroom('playerjoined' , this.get_players_data())  //to refresh dropped player

        });
        socket.on('drop_by_invalid_claim', (data) => {
          clearTimeout(this.timer_claimwinwait) 

          console.log("player dropping",data.username)
          this.dropplayer(data.username,true)
          this.broadcastroom('playerjoined' , this.get_players_data())  //to refresh dropped player

        });

        socket.on('pickedfromdeck', (data) => {
          this.broadcastroom_except_me('pickedfromdeck',data,data.username)
          this.pickedcard = this.pickablecard
          this.pickablecard = this.deckcards[0]
          this.deckcards.splice(0, 1);
          // if deckcard is zero
          if(this.deckcards.length == 0){
            console.log("cards are over ");
            this.deckcards = this.discardedcards
            this.discardedcards = []
          }

        });
        socket.on('pickedfromdiscard', (data) => {
          this.broadcastroom_except_me('pickedfromdiscard',data,data.username)
          this.pickedcard = this.discardedcards[this.discardedcards.length-1]
          this.discardedcards.splice(this.discardedcards.length-1, 1)
        });
        socket.on('discard', (data) => {
          this.pickedcard = null
          var temp = Card.newcard(data.card.number,data.card.color,data.card.isjoker)
          this.discardedcards.push(temp)
          console.log(data)
          this.broadcastroom_except_me('discard',data,data.username)
          clearTimeout(this.timer_nextturn)
          this.nextturn_waitend()


        });
        socket.on('claimwin', (data) => {
          this.pickedcard = null
          clearTimeout(this.timer_nextturn)
          this.broadcastroom_except_me('claimwin',data,data.username)

          this.user_who_claimed_win =  data.username
          this.claimwinwait_starts()


        });
        socket.on('mycards', (data) => {
          console.log(data)
          this.updateplayerpoints(data)

        });
        socket.on('timesend', (data) => {
              console.log("hellooooooooooooooooo")
              console.log("Time lefttttttttttttttttttttttttttttt: "+getTimeLeft(this.timer_nextturn)+"s")
              var timeleft = getTimeLeft(this.timer_nextturn);
              console.log("turn:      " + this.whoseturn)
              this.broadcastroom('turngivento', { 'timeamt': '' + timeleft, 'username': this.players[this.whoseturn].username, "hidden": this.pickablecard, "discarded": this.discardedcards[this.discardedcards.length - 1], "discardedAll": this.discardedcards })

            });
        socket.on('confirmclaimwin', (data) => {
          clearTimeout(this.timer_claimwinwait) 

          this.updateplayerpoints(data)
          this.broadcastroom_except_me('confirmclaimwin',data,data.username)
          this.updateplayer_shown_status(data.username)
          this.update_set_me_won(data.username)
          this.clamwin_starts() 
        }) 
        socket.on('confirmarrangedcards', (data) => {
          this.updateplayerpoints(data)
          this.updateplayer_shown_status(data.username)
          //todo  brodcast cards and 
          this.broadcastroom('confirmarrangedcards',this.get_players_results())
          if(this.checkwheatherallcardsshown() == true){
            this.clamwin_waitend()
          }

        })
        
        
        
        socket.on('exit', (data) => {
            ///////////////////
            if(this.remain_deals != this.tabletype){
              this.addplayerto_playerleft_onexit(socket)

            }
            if(this.matchstatus == Status.started){
              console.log('player leaving while match started');
              if(data.isthismyturn == true){
                this.nextturn_waitend()
              }
                  //if match already started and it is  my turn
            }
             console.log(data.username,':Exit from room:',socket.id)

              this.deleteplayeronexit(socket)
              this.deletesocket(socket)
              main.deleteplayer(data.username)
            
            if( this.players.length == 0 )  //this.users.length == 0 ||
            {
              console.log('closed room onexit:',this.name)
              //if game is started closemthe game first
              main.deleteroom(this)
              //todo entry in database
            }
            if (this.players.length == 1 && this.matchstatus == Status.started) {
		
                this.players[0].amiwon = true
                if (this.players[0].username != 'bot') {
                            console.log("stoppedotherway")
                            //this.stopgame_otherway()
                            this.stopgame(true)

                            //todo entry in database
                        }
                 else
                 {
                            console.log('closed room onexit:', this.name)
                    //
                            //if game is started closemthe game first
                            main.deleteplayer('bot')
                            main.deleteroom(this)
                 }

            }
            else if( this.players.length - this.get_dropped_count() == 1 && this.matchstatus == Status.started){
              this.players[this.get_lastNondrop_index()].amiwon = true
              this.stopgame(true)

            }

          //////////////////////////////
          console.log(data.username,':Exit from roompoints:',socket.id)
          console.log('matchstatus',this.matchstatus.key)

          this.broadcastroom('playerleft' , this.get_players_data())
          if(this.matchstatus == Status.waiting  || this.matchstatus == Status.standby)
          {
            if(this.remain_deals == this.tabletype){
              this.opentojoin = true; 
            }
            
            if(this.players.length ==1)
            {
              if(this.players[0] != 'bot'){
              clearTimeout(this.matchtimeoutObj)
              //clearTimeout(this.reseattimeoutObj)
              this.matchstatus = Status.waiting;
              this.broadcastroom('matchstatus' , {'matchstatus':this.matchstatus.key})
              console.log('waiting for opponenets: roompoints')

              if(this.remain_deals != this.tabletype){
                //this.stopgame(true)
                this.stopgame_otherway()
                
              }
               else
              {
                console.log('closed room onexit:', this.name)
                    //
                            //if game is started closemthe game first
                            main.deleteplayer('bot')
                            main.deleteroom(this)
              }
            }
             


            }
          }
          if(this.matchstatus == Status.reseat){
            clearTimeout(this.reseattimeoutObj)
            if(this.players.length < 2){
               if(this.player[0].username != 'bot')
                {
                    this.opentojoin = true;
                    this.matchstatus = Status.waiting;
                    this.broadcastroom('matchstatus' , {'matchstatus':this.matchstatus.key})

                }
                else
                {
                    main.deleteplayer('bot')
                    main.deleteroom(this)
                    //todo entry in database
            }

            }
            else{
              this.waittimer_reseat()
            }

          }
          if(this.matchstatus == Status.started){
            console.log('left while match started: roompoints')
              //if my turn then  then nextturn start
            //todo take cards , drop
            //todo in case if its players turn 
            


          }
          
          socket.disconnect()
        });
        
        
        this.broadcastroom('playerjoined' , this.get_players_data())

        //todo  forse exit player in case of low balance

      }
      get_players_data(){
        var temparray = [];
        for (var i = 0; i < this.players.length; i++) {
          
            temparray.push({'username': this.players[i].username, 'uid': this.players[i].uid ,'ismyturn':this.players[i].ismyturn ,'isdropped':this.players[i].isdropped,'reseatcard':this.players[i].reseatcard,'dname':this.players[i].dname,'amilast':this.players[i].amilast,'buyin':this.players[i].buyin});
          
        }
        return {"all":temparray}
      }
      get_deal_winner(){
        var leadinguid = 0
        var winnerarray = []
        var minimumpoints = this.players[0].gettotalpoints()
        for(var i= 1 ; i<this.players.length;i++){
          
          var firsttotal = this.players[leadinguid].gettotalpoints()
          var thistotal = this.players[i].gettotalpoints()
          if(thistotal < firsttotal){
            leadinguid = i
            minimumpoints = thistotal;
          }
        }
        for(var i= 0 ; i<this.players.length;i++){
          if(this.players[i].gettotalpoints() == minimumpoints &&this.players[i].username != 'bot'){
            winnerarray.push(this.players[i].username)
          }
        }
         var leadinguid =  this.players[leadinguid].username
         return {'username':winnerarray}
      }
      get_players_results(){
        var leadinguid = 0
        var minpoints = this.players[0].gettotalpoints()
        for(var i= 1 ; i<this.players.length;i++){
          var firsttotal = this.players[leadinguid].gettotalpoints()
          if(this.players[i].username == 'bot')
          {
          var thistotal = this.players[i].gettotalpoints()
          }
          if(thistotal < firsttotal){
            minpoints = thistotal
            leadinguid = i
          }
        }
       
         var leadinguid =  this.players[leadinguid].uid

        var temparray = [];

        for (var i = 0; i < this.players.length; i++) {
          
          if(this.players[i].username == 'bot')
            {            temparray.push({'username': this.players[i].username, 'uid': this.players[i].uid  ,'isdropped':this.players[i].isdropped ,'cards':this.players[i].mycards , 'points':this.players[i].mypoints ,'isshown':this.players[i].mycardsshown,'amiwinner':this.players[i].amiwon,'mytotalpoints':this.players[i].gettotalpoints(),'amileading':this.players[i].gettotalpoints() == minpoints?true:false,'dealsArray':this.players[i].dealsArray,'dname':this.players[i].dname,'buyin':this.players[i].buyin});
          }
          
        }
        for (var i = 0; i < this.playersleft.length; i++) {
          if(this.players[i].username == 'bot')
            {      
          temparray.push({'username': this.playersleft[i].username, 'uid': this.playersleft[i].uid  ,'isdropped':this.playersleft[i].isdropped ,'cards':this.playersleft[i].mycards , 'points':this.playersleft[i].mypoints ,'isshown':this.playersleft[i].mycardsshown,'amiwinner':false ,'mytotalpoints':this.playersleft[i].gettotalpoints(),'amileading':false,'dealsArray':this.playersleft[i].dealsArray,'dname':this.playersleft[i].dname,'buyin':this.playersleft[i].buyin});
        }
        }
        //console.log('results:',temparray)
        return {"all":temparray}
      }
      get_players_reseat_data(){
        var temparray = [];
        for (var i = 0; i < this.players.length; i++) {
          
            temparray.push({'username': this.players[i].username, 'uid': this.players[i].uid ,'ismyturn':this.players[i].ismyturn ,'card':Card.get_random_card()});
          
        }

        return {"all":temparray}
      }
      //only new player calls this function
      check_if_match_can_start(){
        
        if(this.matchstatus == Status.waiting){

          this.standby_match()
        }
        else if(this.matchstatus == Status.standby){
              //todo send msg to myself 
              const t = this.getTimeLeft(this.matchtimeoutObj,this.matchtimeoutObjStarttime)
              console.log('Time left: '+t+'s');
              this.broadcastroom('wait' , { "time":t,'room':this.name})
              this.broadcastroom('matchstatus' , {'matchstatus':this.matchstatus.key})

        }
        if(this.maxtype <= this.players.length)
        {
          
          this.opentojoin  = false ;
        }
      }
      standby_match(){
        if( this.players.length > 1){
          this.matchstatus = Status.standby
          //this.matchstarted = true
          this.broadcastroom('wait' , { "time":this.maxtype == 2 ? 10 : 20,'room':this.name})
          this.broadcastroom('matchstatus' , {'matchstatus':this.matchstatus.key})

          clearTimeout(this.matchtimeoutObj)
          clearTimeout(this.reseattimeoutObj) //not needed
          this.waittimer_startMatch(this.maxtype == 2 ? 10000 : 20000)
          
        }else{
          //this.broadcastroom('msg' , {"msg":'waiting for opponenets'})
        }         
      }
      waittimer_startMatch(sec) {
        this.matchtimeoutObjStarttime = Date.now()
        this.matchtimeoutObj = setTimeout(() => {
        
          if(this.remain_deals == this.tabletype)
          {

            for (var i = 0; i < this.players.length; i++) 
            {
              if(this.players[i].isdisconnected == true)
              {
                

                this.maindeleteplayer(this.players[i].username)
                this.players.splice(i, 1);
                i--;
                this.broadcastroom('playerleft' , this.get_players_data())

              }
          
            }
            if( this.players.length < 2)
            {
              this.broadcastroom('msg' , {"msg":'waiting for opponenets'})
              this.matchstatus = Status.waiting
              this.broadcastroom('matchstatus' , {'matchstatus':this.matchstatus.key})
              return
            }
          }
          
          //if somone left 
          //if(this.maxtype > this.players.length)
         

          //this.matchstarted = true
          console.log('timeout beyond time, match  started')
          this.broadcastroom('msg' , {"msg":'start','room':this.name})

          
          
          if(this.areSeatsRanked == false){

            this.areSeatsRanked = true
            ////shuffle ranks
            this.waittimer_reseat()
        
          }
          else{
            //shuffle cards 
            //send first turn
	    console.log("shuffle started")
            this.shufflecards()


          }
              //start match

        }, sec);

        //clearTimeout(timeoutObj);
      }

      waittimer_reseat() {
        
        this.matchstatus = Status.reseat
        this.broadcastroom('matchstatus' , {'matchstatus':this.matchstatus.key})
        this.broadcastroom('reseat' , {"time":10 ,'room':this.name})

        this.matchtimeoutObj = setTimeout(() => {
          if( this.players.length < 2)
            {
              this.broadcastroom('msg' , {"msg":'waiting for opponenets'})
              this.matchstatus = Status.waiting
              this.broadcastroom('matchstatus' , {'matchstatus':this.matchstatus.key})
              return
            }
            
          this.reseat_player_sort()
          this.broadcastroom('playerjoined' , this.get_players_data())  //to refresh dropped player

          console.log('reseat timeouttt')
          this.shufflecards()
        },3000)
      }
      shufflecards(){
	console.log("shuff")
         if (this.remain_deals == this.tabletype) {
            this.remain_deals = this.players.length
            this.remain_deals = this.remain_deals - 1
            //
            for (var i = 0; i < this.players.length; i++) {
                this.players[i].buyin = 80 * this.players.length
            }

        } else {
            this.remain_deals = this.remain_deals - 1

        }

        console.log("here")

        var board = Card.getshuffledcards(false,this.maxtype,true)
        this.jokercard = board[1]
        this.deckcards = board[0]
        this.papluJoker = board[2] 
        this.pickablecard = this.deckcards[0]
        this.deckcards.splice(0, 1);
	console.log("here")
        this.discardedcards = []
        this.discardedcards.push(this.deckcards[0])
        this.deckcards.splice(0, 1);
	console.log("here")
        
        for(var i = 0 ; i<this.players.length ;i++){
          var cardstosend = []
          if(i==0){
            //cardstosend.push(this.deckcards[0])
            //this.deckcards.splice(0, 1);
          }
          for(var j=0 ;j<13;j++){
            cardstosend.push(this.deckcards[0])
            this.deckcards.splice(0, 1);
          }
          this.players[i].mycards = cardstosend;
          this.players[i].ismyturn = false
          this.players[i].amilast = false
	        if(this.players[i].username != 'bot') {
          this.players[i].socket.emit('start' , {"isdropped":this.players[i].isdropped,"joker":this.jokercard,"hidden":this.pickablecard,"discarded":this.discardedcards,"cards":this.players[i].mycards,"papluJoker":board[2]});
          }//console.log(' cards sent to',this.players[i].username,' are ',cardstosend)
        }
        console.log('remaining cards are',this.deckcards,'length',this.deckcards.length)
        if (this.moneytype == 'paid')
                   {
                   for(var z=0; z<this.players.length;z++)
                   {
                    var deductamt  = this.tabletype * 80
                    db.addmoney(this.players[z].uid , -deductamt )
                    console.log("players uid:" + this.players[z].uid)
                   }
                 }

       // this.broadcastroom('start' , {"joker":this.jokercard,"hidden":this.deckcards[0],"discarded":this.deckcards[1]})
        this.matchstatus = Status.started
        this.broadcastroom('matchstatus' , {'matchstatus':this.matchstatus.key})
        var turngivenint  =  (this.remain_deals+1)  %   this.players.length
        console.log('turngivenint:',turngivenint)
        //this.timeamt = this.players[turngivenint].get_time_amt()
        var amt =  45//this.isthisfirstround == true ? 45:this.players[turngivenint].get_time_amt()
        this.timeamt = amt
        this.whoseturn = 0
        this.broadcastroom('turngivento',{'timeamt':''+amt,'username':this.players[turngivenint].username ,"hidden":this.pickablecard,"discarded":this.discardedcards[this.discardedcards.length-1],"discardedAll":this.discardedcards})
        this.players[turngivenint].ismyturn = true;
        this.players[turngivenint==this.players.length-1?0:turngivenint+1].amilast = true;
        console.log('playercount:',this.players.length,this.playersleft.length)

        this.broadcastroom('playercount',{'playercount':''+(this.players.length + this.playersleft.length)})
        this.nextturn_start()
        this.opentojoin  = false 
        this.broadcastroom('playerjoined' , this.get_players_data())


      }
      claimwinwait_starts(){
        clearTimeout(this.timer_claimwinwait)
        this.timer_claimwinwait = setTimeout(() => {

          console.log("player dropping",this.user_who_claimed_win)
          this.dropplayer(this.user_who_claimed_win,true)
          this.broadcastroom('playerjoined' , this.get_players_data())

          this.broadcastroom('forcedrop',{'username':this.user_who_claimed_win})

          this.nextturn_waitend()
        },30000)
      }
      nextturn_start() 
      {
        clearTimeout(this.timer_nextturn)
        this.timer_nextturn = setTimeout(() => {
          this.nextturn_waitend(true)
        },this.timeamt*1000)
      }
      nextturn_waitend(isturnmissed){
        clearTimeout(this.timer_nextturn)
        for(var i= 0 ; i<this.players.length;i++){
          if(this.players[i].ismyturn == true){
            if(isturnmissed == true){
              this.players[i].increment_turn_missed()
              if(this.players[i].get_turn_missed() == 3 && this.players[i].autodrop == true){
                this.dropplayer(this.players[i].username,false)
                this.broadcastroom('playerjoined' , this.get_players_data())
                return
              }
              
            } else{
              this.players[i].reset_turn_missed()
            }
            if(this.pickedcard != null){
              //means player picked card but failed to discard

              this.broadcastroom('forcediscard',{'card':this.pickedcard , 'username':this.players[i].username})
              this.discardedcards.push(this.pickedcard)
              this.pickedcard = null

            }

            for(var j= 0 ; j<this.players.length;j++){
              this.players[j].ismyturn = false
            }
            for(var j= 0 ; j<this.players.length;j++){
              i--;
              if(this.turncounter >= this.players.length){
                this.isthisfirstround = false
              }
              if(i==-1){
                i= this.players.length-1

              }

              if(this.players[i].isdropped == false){
                this.players[i].ismyturn = true
                //this.timeamt = this.players[i].get_time_amt()
                this.turncounter = this.turncounter + 1
                var amt =  this.isthisfirstround == true ? 45:this.players[i].get_time_amt()
                this.timeamt = amt
                if (this.players[i].username == 'bot') {
                        this.timeamt  =  5
                        //clearTimeout(this.timer_nextturn)
                        //this.broadcastroom('turngivento', { 'timeamt': ''+this.timeamt, 'username': 'bot', "hidden": this.pickablecard, "discarded": this.discardedcards[this.discardedcards.length - 1], "discardedAll": this.discardedcards })
                        timeout_time = setTimeout(() => {
                            i--;
                            j++;

                            this.broadcastroom_except_me('pickedfromdeck', { "username": "bot" }, 'bot')
                            this.pickedcard = this.pickablecard
                            this.pickablecard = this.deckcards[0]
                            this.deckcards.splice(0, 1)
                            // if deckcard is zero
                            if (this.deckcards.length == 0) {
                                console.log("cards are over ")
                                this.deckcards = this.discardedcards
                                this.discardedcards = [] 
                            
                                this.pickedcard = null
                                var temp = Card.newcard(this.pickablecard)
                                this.discardedcards.push(temp)
                                //console.log(data)
                                this.broadcastroom_except_me('discard', { "username": "bot", "card": this.pickablecard }, 'bot')
                                //clearTimeout(this.timeout_time)
                                // console.log("starting discaard function")
                                
                                
                                //this.nextturn_waitend()
                                //this.nextturn_start()                                                                                             //this.players[a].socket.emit('pickedfromdeck' , {"username":"bot"})                                                                                                        //this.players[a].socket.emit('discard' , {"username":"bot","card":{"color":2,"number":5,"isjoker":false}})
                           }
                            //this.datat = {"username": 'bot',"cards": this.botcard , "mypoints": 82, "hidden": { "color": 0, "number": 0, "isjoker": false } }
                            //this.updateplayerpoints(this.datat)
                            this.timeamt = 5
                            this.broadcastroom('turngivento', { 'timeamt': ''+25, 'username': 'bot', "hidden": this.pickablecard, "discarded": this.discardedcards[this.discardedcards.length - 1], "discardedAll": this.discardedcards })
                            
                        }, this.timeamt)
                        this.timeamt = 0.1
                        
                        break
                        //this.nextturn_waitend(false)
                        //break
                    }


                else{
                this.broadcastroom('turngivento',{'timeamt':''+amt,'username':this.players[i].username,"hidden":this.pickablecard,"discarded":this.discardedcards[this.discardedcards.length-1],"discardedAll":this.discardedcards})
                  break
                }
              }
            }

            // if(i == 0){
            //   this.players[this.players.length-1].ismyturn = true
            //   this.broadcastroom('turngivento',{'username':this.players[this.players.length-1].username,"hidden":this.pickablecard,"discarded":this.discardedcards[this.discardedcards.length-1]})

            // }
            // else{
            //   if(i-1 ==0){
            //     this.isthisfirstround = false
            //     console.log('second round')
            //   }
            //   this.players[i-1].ismyturn = true
            //   this.broadcastroom('turngivento',{'username':this.players[i-1].username,"hidden":this.pickablecard,"discarded":this.discardedcards[this.discardedcards.length-1]})

            // }
            this.nextturn_start()
            break;

          
          }
        }
      }

      clamwin_starts() 
      {
        clearTimeout(this.timer_clamwin)
        this.timer_clamwin = setTimeout(() => {
          this.clamwin_waitend()
        },30000)
      }
      clamwin_waitend(){
        clearTimeout(this.timer_clamwin)
        this.stopgame(false)

        

      }
      stoptimers(){
        if (this.matchtimeoutObj) {
          clearInterval(this.matchtimeoutObj);
          this.matchtimeoutObj = null;
        }
        if (this.reseattimeoutObj) {
          clearInterval(this.reseattimeoutObj);
          this.reseattimeoutObj = null;
        }
        if (this.timer_nextturn) {
          clearInterval(this.timer_nextturn);
          this.timer_nextturn = null;
        }
        if (this.timer_clamwin) {
          clearInterval(this.timer_clamwin);
          this.timer_clamwin = null;
        }
        if (this.timer_claimwinwait) {
          clearInterval(this.timer_claimwinwait);
          this.timer_claimwinwait = null;
        }
      }
      



      stopgame(should_stop_fullgame){
        console.log('stopping the game');
        //update database
        this.update_winner_in_database(should_stop_fullgame)
        this.resetvalues()
        //broadcast results
	console.log("ok")
        this.restartmatch()
      }
      restartmatch(){
        clearInterval(this.restartmatchtimeoutObj)
        this.restartmatchtimeoutObj = setTimeout(() => {
          console.log('reseat timeout')
          this.check_insufficient_balance(this.entryfee)

          this.matchstatus = Status.waiting
          this.check_if_match_can_start()
          this.broadcastroom('playerjoined' , this.get_players_data())  //to refresh dropped player


        },3000)
      }
      update_winner_in_database(should_stop_fullgame)
      {
        var winnerindex = -1
         var totalwinningpoints = 0
         var holdingmoneyback = this.tabletype * 80
        for(var i= 0 ; i<this.players.length;i++){

              this.players[i].mypoints   =Math.floor(Math.random() * Math.floor(60 - 15) + 15)
              console.log("herepointsxx" +this.players[i].mypoints )
              if(this.players[i].username != 'bot')
              {
              this.players[i].dealsArray.push(this.players[i].mypoints)
              //this.players[i].mypoints = 60
            }
            
            if (this.moneytype == 'paid')
            {
          db.addmoney(this.players[i].uid, holdingmoneyback)
            }
          console.log("cccccccccc")
          if(this.players[i].amiwon == true && winnerindex == -1){
            winnerindex = i
            this.players[i].mypoints = 0
            if(this.players[i].username != 'bot')
              {
            this.players[i].dealsArray.push(0)
          }
             console.log("cccccccccc")
          }
          else{
            if(this.players[i].mypoints>80){
              this.players[i].mypoints = 80
              
            }
            if(this.players[i].mypoints == 0){
              this.players[i].mypoints = 2
            }
            this.players[i].update_buyin(0-this.players[i].mypoints)
            if(this.players[i].username != 'bot')
              {
            this.players[i].dealsArray.push(this.players[i].mypoints)
          }
            totalwinningpoints = totalwinningpoints + this.players[i].mypoints
            console.log('player:',this.players[i].username," lost ",this.players[i].mypoints,' points')
             //console.log("cccccccccc")
            //todo update loser in database
          }
           console.log("cccccccccc")

        }
        for(var i= 0 ; i<this.playersleft.length;i++){
         /* if(this.playersleft[i].username == 'bot')
              {
                this.playersleft[i].mypoints   =Math.floor(Math.random() * Math.floor(70))
                console.log("herepoints" +this.playersleft[i].mypoints )
                //this.playersleft[i].mypoints = 60
              }*/
              console.log("cccccccccc")
          if (this.moneytype == 'paid')
               {
          db.addmoney(this.playersleft[i].uid, holdingmoneyback)
          }
          if(this.playersleft[i].mypoints>80){
            this.playersleft[i].mypoints = 80
            //this.playersleft[i].dealsArray.push(80)
          }
          if(this.playersleft[i].mypoints == 0){
            this.playersleft[i].mypoints = 2
          }
          //this.playersleft[i].dealsArray.push(this.playersleft[i].mypoints)
          totalwinningpoints = totalwinningpoints + this.playersleft[i].mypoints
          console.log('player:',this.playersleft[i].username,"lost",this.playersleft[i].mypoints,'points')
           //todo update loser in database

        }
        if(winnerindex > -1){
          console.log('winner for this match is:',this.players[winnerindex].username)
          console.log('and winning points are :',totalwinningpoints)
          //todo update winner database
          this.players[winnerindex].update_buyin(totalwinningpoints)
        }
        if(should_stop_fullgame == true){
          this.remain_deals = 0
        }
        
        if(this.remain_deals == 0){
          var winners = this.get_deal_winner()
          if(winners['username'].length<2){
            
            this.broadcastroom('winner',this.get_deal_winner())
            db.insertgameresult_deals(this.maxtype,this.tabletype,this.moneytype,this.number,this.players,this.playersleft,this.entryfee)
 
          }
          else{
            console.log("multiple winners")
            this.remain_deals++
            this.broadcastroom('DealTie' , {'a':'a'})

          }

          
        }
        var botexpired =false
        //console.log("arrrrray" + this.players[0].dealsArray + "length" + this.players[0].dealsArray.length)
        for(var z=0; z<this.players.length ; z++)
        {
          if(this.players[i].username != 'bot')
              {
                botexpired = true
              }
        }
        if(botexpired == true)
              {
        var dealcount = this.players[0].dealsArray.length
        this.broadcastroom('result2',{"players":this.get_players_results(),"joker":this.jokercard,"dealcount":dealcount})
      }
      else
      {
        //var dealcount  = 1
        this.broadcastroom_except_me('result2',{"players":this.get_players_results(),"joker":this.jokercard,"dealcount":5})
      }
     
        
        
       // this.broadcastroom('result',{"players":this.get_players_results(),"joker":this.jokercard,"totalwinningpoints":totalwinningpoints})
      }

      stopgame_otherway(){
        this.remain_deals = 0
        this.broadcastroom('winner',this.get_deal_winner())
           db.insertgameresult_deals(this.maxtype,this.tabletype,this.moneytype,this.number,this.players,this.playersleft,this.entryfee)
          this.resetvalues()
         this.matchstatus = Status.waiting;
         this.broadcastroom('matchstatus' , {'matchstatus':this.matchstatus.key})

      }

}
function getTimeLeft(timevar) {
    return Math.ceil((timevar._idleStart + timevar._idleTimeout) / 1000 - process.uptime());
}