// class OneCard{
//     constructor(number,color,isjoker)
//     {
//
//         this.color = color
//         if(color == 4){
//             this.number = 13
//             this.isjoker = true
//
//         }
//         else{
//             this.number = number
//             this.isjoker = isjoker
//         }
//
//     }
// }
//
// function getRandomInt(max)
// {
//     return Math.floor(Math.random() * Math.floor(max))
// }
// module.exports.newcard = function newcard(number,color,isjoker){
//
//     const card = new OneCard(number,color,isjoker)
//     return card
// }
// module.exports.get_random_card = function get_random_card(){
//     var jno = getRandomInt(13)
//     var jcol = getRandomInt(4)
//     const jokerrandom = getRandomInt(53)   //probablity 1 in 53
//     if(jokerrandom ==1){
//         jcol = 4
//         jno = 13
//     }
//
//     const card = new OneCard(jno,jcol,false)
//     return card
// }
//
// module.exports.getshuffledcards = function getshuffledcards(onecard,maxtype,isdeal){
//     allcards = []
//     //joker card
//     var jno = getRandomInt(13)
//     var jcol = getRandomInt(4)
//     const jokerrandom = getRandomInt(53)   //probablity 1 in 53
//     if(jokerrandom ==1){
//         jcol = 4
//     }
//
//     var onecard_jno = jno == 12 ? 0 : jno + 1
//     var onecard_jcol =  jcol
//     if(jcol == 4){
//         onecard_jcol = 0
//         onecard_jno = 1
//     }
//    // var onecardJoker = new Card(onecard_jno,onecard_jcol,true)
//     var jokertoshow = new OneCard(jno,jcol,true)
//
//     if(jcol == 4 )
//     {
//         jcol = 0
//         jno = 0
//         //create one jokers
//         const jokercard = new OneCard(0,0,true)
//         allcards.push(jokercard)
//
//         if(maxtype == 10 || isdeal == false){
//             const joker1 = new OneCard(0,4,true)
//             allcards.push(joker1)
//
//
//         }
//
//     }
//     else
//     {
//
//         //create two jokers
//         const joker1 = new OneCard(0,4,true)
//         allcards.push(joker1)
//
//     }
//     for(var i = 0 ;i<4;i++)
//     {
//         for(var j = 0; j<13;j++)
//         {
//             if(jno == j )
//             {
//                 if(maxtype == 10 || isdeal == false){
//                     const c1 =  new OneCard(j,i,true)
//                     allcards.push(c1)}
//                 if(jcol != i)
//                 {
//                     const c2 =  new OneCard(j,i,true)
//                     allcards.push(c2)
//                 }
//
//             }
//             else{
//                 if(onecard == true){
//                     if(onecard_jno == j && onecard_jcol == i){
//                         if(maxtype == 10 || isdeal == false){
//                             const c1 =  new OneCard(j,i,true)
//                             allcards.push(c1)}
//
//                         const c2 =  new OneCard(j,i,true)
//                         allcards.push(c2)
//
//                     }else
//                     {
//                         if(maxtype == 10 || isdeal == false){
//                             const c1 =  new OneCard(j,i,false)
//                             allcards.push(c1)}
//
//                         const c2 =  new OneCard(j,i,false)
//                         allcards.push(c2)
//                     }
//                 }
//                 else{
//                     if(maxtype == 10 || isdeal == false){
//                         const c1 =  new OneCard(j,i,false)
//                         allcards.push(c1)}
//
//                     const c2 =  new OneCard(j,i,false)
//                     allcards.push(c2)
//                 }
//
//             }
//         }
//     }
//     //console.log(allcards.length)
//     //console.log(allcards)
//     //console.log('joker',jokertoshow)
//
//     shuffledcards = []
//     var x = maxtype==10 || isdeal == false?53:52
//     for(var i = 0 ; i<x ;i++)
//     {
//         const random = getRandomInt(allcards.length)
//         shuffledcards.push(allcards[random])
//         allcards.splice(random, 1);
//         // console.log(" cards ",shuffledcards[i])
//     }
//
//     //console.log("shuffled cards are",shuffledcards)
//
//
//     return [shuffledcards, jokertoshow,papluJoker]
//
// }
// module.exports.OneCard = OneCard
//
//
//
// /*
//     spade,
//     heart,
//     diamond,
//     club,
//     joker
//
//     pure,
//     sequence,
//     set,
//     invalid,
//     joker
//
//
//     */
//
// //create joker card
// // const no = getRandomInt(13)
// // const col = getRandomInt(4)
// // const jokertoshow = new Card(no,col,true)


module.exports.get_random_card = function get_random_card(){
    var jno = getRandomInt(13)
    var jcol = getRandomInt(4)
    const jokerrandom = getRandomInt(105)   //probablity 1 in 105
    if(jokerrandom ==1){
        jcol = 4
        jno = 13
    }

    const card = new Card(jno,jcol,false)
    return card
}