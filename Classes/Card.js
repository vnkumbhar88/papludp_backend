    class Card{
        constructor(number,color,isjoker)
        {
            
            this.color = color
            if(color == 4){
                this.number = 13
                this.isjoker = true

            }
            else{
                this.number = number
                this.isjoker = isjoker
            }
            
        }
    }

        function getRandomInt(max) 
        {
            return Math.floor(Math.random() * Math.floor(max))
        }
        module.exports.newcard = function newcard(number,color,isjoker){
            
            const card = new Card(number,color,isjoker)
            return card
        }
        module.exports.get_random_card = function get_random_card(){
            var jno = getRandomInt(13)
            var jcol = getRandomInt(4)
            const jokerrandom = getRandomInt(105)   //probablity 1 in 105
            if(jokerrandom ==1){
                jcol = 4
                jno = 13
            }

            const card = new Card(jno,jcol,false)
            return card
        }

        module.exports.getshuffledcards = function getshuffledcards(papludp,maxtype,isdeal){
        allcards = []
        //joker card
        var jno = getRandomInt(13)
        var jcol = getRandomInt(4)
            const jokerrandom = getRandomInt(105)   //probablity 1 in 105
            if(jokerrandom ==1){
                jcol = 4
            }
       
        var paplu_jno = jno == 12 ? 0 : jno + 1
        var paplu_jcol =  jcol
        if(jcol == 4){
            paplu_jcol = 0
            paplu_jno = 1
        }
        var papluJoker = new Card(paplu_jno,paplu_jcol,true)
        var jokertoshow = new Card(jno,jcol,true)

        if(jcol == 4 )
        {
            jcol = 0
            jno = 0
            //create one jokers 
            const jokercard = new Card(0,0,true)
            allcards.push(jokercard)

            if(maxtype == 6 || isdeal == false){
            const joker1 = new Card(0,4,true)
            allcards.push(joker1)

            
             }
            
        }
        else
        {
            
            //create two jokers
            const joker1 = new Card(0,4,true)
            const joker2 = new Card(0,4,true)
            allcards.push(joker1)
            if(maxtype == 6 || isdeal == false)
            {
            allcards.push(joker2)
            }
            
        }
        for(var i = 0 ;i<4;i++)
        {
            for(var j = 0; j<13;j++)
            {
                if(jno == j )
                {
                    if(maxtype == 6 || isdeal == false){
                    const c1 =  new Card(j,i,true)
                    allcards.push(c1)}
                    if(jcol != i)
                    {
                        const c2 =  new Card(j,i,true)
                        allcards.push(c2)
                    }

                }
                else{
                    if(papludp == true){
                        if(paplu_jno == j && paplu_jcol == i){
                            if(maxtype == 6 || isdeal == false){
                            const c1 =  new Card(j,i,true)
                            allcards.push(c1)}
            
                            const c2 =  new Card(j,i,true)
                            allcards.push(c2)

                        }else
                        {
                            if(maxtype == 6 || isdeal == false){
                            const c1 =  new Card(j,i,false)
                            allcards.push(c1)}
            
                            const c2 =  new Card(j,i,false)
                            allcards.push(c2)
                        }
                    }
                    else{
                        if(maxtype == 6 || isdeal == false){
                        const c1 =  new Card(j,i,false)
                        allcards.push(c1)}
        
                        const c2 =  new Card(j,i,false)
                        allcards.push(c2)
                    }
               
                }
            }
        }
        //console.log(allcards.length)
        //console.log(allcards)
        //console.log('joker',jokertoshow)

        shuffledcards = []
        var x = maxtype==6 || isdeal == false?105:52
        for(var i = 0 ; i<x ;i++)
        {
            const random = getRandomInt(allcards.length)
            shuffledcards.push(allcards[random])
            allcards.splice(random, 1);
        // console.log(" cards ",shuffledcards[i])
        }

        //console.log("shuffled cards are",shuffledcards)


        return [shuffledcards, jokertoshow,papluJoker]
    
    }
    module.exports.Card = Card



    /*
        spade,
        heart,
        diamond,
        club,
        joker
        
        pure,
        sequence,
        set,
        invalid,
        joker
        
        
        */

        //create joker card
        // const no = getRandomInt(13)
        // const col = getRandomInt(4)
        // const jokertoshow = new Card(no,col,true)