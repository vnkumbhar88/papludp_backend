const Player = require('./Player')
const db = require('../model/roomsql');

module.exports = class PlayerPapluDp extends Player{

    constructor(socket,u_id ,username,gametype  ,maxtype,moneytype,tabletype,dname,autodrop)
    {
        super(socket,u_id ,username,gametype  ,maxtype,moneytype,dname,autodrop)
        this.tabletype = tabletype  //0.1 rs 10 rs 
        this.assign_buyin()

        
    }

    get_no_of_paplu_joker(jno,jcol){
        var paplu_jno = jno == 12 ? 0 : jno + 1
        var paplu_jcol =  jcol
        if(jcol == 4){
            paplu_jcol = 0
            paplu_jno = 1
        }


        var paplucount = 0
        for(var i = 0; i < this.mycards.length ; i++){
            if(this.mycards[i].number == paplu_jno && this.mycards[i].color == paplu_jcol){
                paplucount++
            }
           
        }
        return  paplucount
    }

    assign_buyin(){
        console.log("assing buy in papludp Player")
        var that = this
        if(this.moneytype == "paid"){
            db.getuserbalance(this.uid,function(bal){
                if(that.tabletype*240 > bal){
                    that.buyin =  Math.floor(bal/that.tabletype)
                }
                else{
                    that.buyin = 240

               }
            })
        }
        else{
            this.buyin = 240
        }
    }
    update_buyin(value){
        this.buyin = this.buyin + value
        if(this.buyin <= 0 ){
            this.assign_buyin()
        }
    }
}